package com.soffice.clickandpay.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.soffice.clickandpay.Activities.Contacts_Activity;
import com.soffice.clickandpay.Utilty.Display;


public class IndexableListView extends ListView {

	private boolean mIsFastScrollEnabled = false;
	private IndexScroller mScroller = null;
	private GestureDetector mGestureDetector = null;
	private Context mcontext;;

	public IndexableListView(Context context) {
		super(context);
		mcontext = context;
	}

	public IndexableListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mcontext = context;
	}

	public IndexableListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mcontext = context;
	}

	@Override
	public boolean isFastScrollEnabled() {
		return mIsFastScrollEnabled;
	}

	@Override
	public void setFastScrollEnabled(boolean enabled) {
		mIsFastScrollEnabled = enabled;
		if (mIsFastScrollEnabled) {
			if (mScroller == null)
				Display.DisplayLogI("ADITYA", "setFastScrollEnabled");
				mScroller = new IndexScroller(mcontext, IndexableListView.this);
		} else {
			if (mScroller != null) {
				mScroller = null;
			}
		}
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		if (mIsFastScrollEnabled) {
			// Overlay index bar
			if (mScroller != null)
				mScroller.draw(canvas);

		}
	}


	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// Intercept ListView's touch event
		try {

			if (mScroller != null && mScroller.onTouchEvent(ev))
				return true;

			if (mGestureDetector == null) {
				mGestureDetector = new GestureDetector(getContext(),
						new GestureDetector.SimpleOnGestureListener() {

							@Override
							public boolean onFling(MotionEvent e1,
									MotionEvent e2, float velocityX,
									float velocityY) {
								// If fling happens, index bar shows
								return super.onFling(e1, e2, velocityX,
										velocityY);
							}

						});
			}
			mGestureDetector.onTouchEvent(ev);

			return super.onTouchEvent(ev);
		} catch (Exception e) {
			return super.onTouchEvent(ev);
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return true;
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
		if (mScroller != null)
			mScroller.setAdapter(adapter);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Contacts_Activity.h = h;
		Contacts_Activity.w = w;
		Contacts_Activity.hm = oldh;
		Contacts_Activity.wm = oldw;

		if (mScroller != null)
			mScroller.onSizeChanged(w, h, oldw, oldh);
	}

}
