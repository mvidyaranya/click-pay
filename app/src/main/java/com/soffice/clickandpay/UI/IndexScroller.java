package com.soffice.clickandpay.UI;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.SectionIndexer;

import com.soffice.clickandpay.Activities.Contacts_Activity;
import com.soffice.clickandpay.Utilty.Display;

import java.util.Arrays;

public class IndexScroller {

    private float mIndexbarWidth;
    private float mIndexbarMargin;
    private float mPreviewPadding;
    private float mDensity;
    private float mScaledDensity;
    private int mListViewWidth;
    private int mListViewHeight;
    private int mCurrentSection = -1;
    private boolean mIsIndexing = false;
    private ListView mListView = null;
    private SectionIndexer mIndexer = null;
    private String[] mSections = null;
    private RectF mIndexbarRect;
    Context mContext;
    Canvas canva;

    public IndexScroller(Context context, ListView lv) {
        mDensity = context.getResources().getDisplayMetrics().density;
        mScaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        mListView = lv;
        setAdapter(mListView.getAdapter());
        mContext = context;
        mIndexbarWidth = 25 * mDensity;
        mIndexbarMargin = 0 * mDensity;
        mPreviewPadding = 0;
    }

    public void draw(Canvas canvas) {
        canva = canvas;
        // mAlphaRate determines the rate of opacity
        Paint indexbarPaint = new Paint();
        // int color = R.color.index_bk;
        // To set Background Color
        indexbarPaint.setColor(Color.parseColor("#f6f6f6"));

        // indexbarPaint.setAlpha((int) (140));
        indexbarPaint.setAntiAlias(true);
        if (mIndexbarRect != null) {
            canvas.drawRoundRect(mIndexbarRect, 0 * mDensity, 0 * mDensity,
                    indexbarPaint);
        } else {
            mListViewWidth = Contacts_Activity.w;
            mListViewHeight = Contacts_Activity.h;
            mIndexbarRect = new RectF(Contacts_Activity.w - mIndexbarMargin - mIndexbarWidth,
                    mIndexbarMargin, Contacts_Activity.w - mIndexbarMargin, Contacts_Activity.h - mIndexbarMargin);

            canvas.drawRoundRect(mIndexbarRect, 0 * mDensity, 0 * mDensity,
                    indexbarPaint);
        }

        if (mSections != null && mSections.length > 0) {
            // Preview is shown when mCurrentSection is set
            if (mCurrentSection >= 0) {
                Paint previewPaint = new Paint();
                previewPaint.setColor(Color.RED);
                previewPaint.setAlpha(96);
                previewPaint.setAntiAlias(true);
                // previewPaint.setShadowLayer(3, 0, 0, Color.argb(64, 0, 0,
                // 0));

                Paint previewTextPaint = new Paint();
                previewTextPaint.setColor(Color.WHITE);
                previewTextPaint.setAntiAlias(true);
                previewTextPaint.setTextSize(50 * mScaledDensity);
                Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                        "fonts/Roboto-Medium.ttf");
                previewTextPaint.setTypeface(tf);

                float previewTextWidth = previewTextPaint
                        .measureText(mSections[mCurrentSection]);
                float previewSize = 2 * mPreviewPadding
                        + previewTextPaint.descent()
                        - previewTextPaint.ascent();
                RectF previewRect = new RectF(
                        (mListViewWidth - previewSize) / 2,
                        (mListViewHeight - previewSize) / 2,
                        (mListViewWidth - previewSize) / 2 + previewSize,
                        (mListViewHeight - previewSize) / 2 + previewSize);

                canvas.drawRoundRect(previewRect, 5 * mDensity, 5 * mDensity,
                        previewPaint);
                canvas.drawText(
                        mSections[mCurrentSection],
                        previewRect.left + (previewSize - previewTextWidth) / 2
                                - 1,
                        previewRect.top + mPreviewPadding
                                - previewTextPaint.ascent() + 1,
                        previewTextPaint);
            }

            Paint indexPaint = new Paint();
            // int color=R.color.index_bk;
            // TO Set Text Details
            indexPaint.setColor(Color.parseColor("#868686"));
            indexPaint.setAlpha((int) (255 * 1));
            indexPaint.setAntiAlias(true);
            indexPaint.setTextSize(15 * mScaledDensity);
            Typeface tf = Typeface.createFromAsset(mContext.getAssets(),
                    "fonts/Roboto-Medium.ttf");
            indexPaint.setTypeface(tf);
            float sectionHeight = (mIndexbarRect.height() - 2 * mIndexbarMargin)
                    / mSections.length;
            float paddingTop = (sectionHeight - (indexPaint.descent() - indexPaint
                    .ascent())) / 2;
            for (int i = 0; i < mSections.length; i++) {
                if (!mSections[i].equalsIgnoreCase("#")) {
                    float paddingLeft = (mIndexbarWidth - indexPaint
                            .measureText(mSections[i])) / 2;
                    canvas.drawText(mSections[i], mIndexbarRect.left
                                    + paddingLeft,
                            mIndexbarRect.top + mIndexbarMargin + sectionHeight
                                    * i + paddingTop - indexPaint.ascent(),
                            indexPaint);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // If down event occurs inside index bar region, start indexing
                if (contains(ev.getX(), ev.getY())) {

                    // It demonstrates that the motion event started from index bar
                    mIsIndexing = true;
                    // Determine which section the point is in, and move the list to
                    // that section
                    mCurrentSection = getSectionByPoint(ev.getY());
                    mListView.setSelection(mIndexer
                            .getPositionForSection(mCurrentSection));
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (mIsIndexing) {
                    // If this event moves inside index bar
                    if (contains(ev.getX(), ev.getY())) {
                        // Determine which section the point is in, and move the
                        // list to that section
                        mCurrentSection = getSectionByPoint(ev.getY());
                        mListView.setSelection(mIndexer
                                .getPositionForSection(mCurrentSection));
                    }
                    return true;
                }

                break;
            case MotionEvent.ACTION_UP:
                Display.DisplayLogI("ADITYA", "ACTION_UP");
                if (mIsIndexing) {
                    mIsIndexing = false;
                    mCurrentSection = -1;
                }

                break;
        }
        return false;
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        mListViewWidth = w;
        mListViewHeight = h;
        mIndexbarRect = new RectF(w - mIndexbarMargin - mIndexbarWidth,
                mIndexbarMargin, w - mIndexbarMargin, h - mIndexbarMargin);
    }

    public void setAdapter(Adapter adapter) {
        Display.DisplayLogI("ADITYA", "SCRLL setAdapter " + adapter);
        if (adapter instanceof SectionIndexer) {
            mIndexer = (SectionIndexer) adapter;
            mSections = (String[]) mIndexer.getSections();
            Display.DisplayLogI("ADITYA", "SCRLL setAdapter adapter: " + adapter + " mIndexer: " + mIndexer + " mSections: " + Arrays.toString(mSections));
        }
    }

    private boolean contains(float x, float y) {
        // Determine if the point is in index bar region, which includes the
        // right margin of the bar
        return (x >= mIndexbarRect.left && y >= mIndexbarRect.top && y <= mIndexbarRect.top
                + mIndexbarRect.height());
    }

    private int getSectionByPoint(float y) {
        if (mSections == null || mSections.length == 0)
            return 0;
        if (y < mIndexbarRect.top + mIndexbarMargin)
            return 0;
        if (y >= mIndexbarRect.top + mIndexbarRect.height() - mIndexbarMargin)
            return mSections.length - 1;
        return (int) ((y - mIndexbarRect.top - mIndexbarMargin) / ((mIndexbarRect
                .height() - 2 * mIndexbarMargin) / mSections.length));
    }

}
