package com.soffice.clickandpay.Services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.soffice.clickandpay.Utilty.Display;


public class ObserverService extends Service {
	private static final Uri Contacts_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
	private ConatctOberserLowVersions contectslow;
	ObserverService mService;
	boolean mIsBound;

	private final IBinder mBinder = new LocalBinder();

	class MyServiceConnection implements ServiceConnection {

		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = ((ObserverService.LocalBinder) service).getService();
			mIsBound = true;

		}

		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mIsBound = false;
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();

		cancelContentObservers();
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN){
			Display.DisplayLogI("ADITYA", "LOW VERSION");
			if (contectslow == null) {
				contectslow = new ConatctOberserLowVersions(new Handler(),
						getBaseContext());
				getApplicationContext().getContentResolver()
						.registerContentObserver(Contacts_URI, true,
								contectslow);
			}
		}else{
			Display.DisplayLogI("ADITYA", "HIGH VERSION");
			if (contectslow == null) {
				contectslow = new ConatctOberserLowVersions(new Handler(),
						getApplicationContext());
				getApplicationContext().getContentResolver()
						.registerContentObserver(Contacts_URI, true,
								contectslow);
			}
		}
		Display.DisplayLogI("ADITYA", "OBERSER SERVICE");

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Display.DisplayLogI("ADITYA", "onStartCommand");

		return START_STICKY;
	}

	@Override
	public void onStart(Intent intent, int startid) {

	}

	@Override
	public void onDestroy() {
		cancelContentObservers();
		super.onDestroy();
	}

	public class LocalBinder extends Binder {
		public ObserverService getService() {
			return ObserverService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		onRebind(intent);
		return false;
	}

	private void cancelContentObservers() {
//		if (smsobserver != null) {
//			getContentResolver().unregisterContentObserver(smsobserver);
//			smsobserver = null;
//		}

//		if (contactobserver != null) {
//			getContentResolver().unregisterContentObserver(contactobserver);
//			contactobserver = null;
//		}
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		super.onTaskRemoved(rootIntent);
		Display.DisplayLogI("ADITYA", "OBERSER SERVICE onTaskRemoved");
	}
}
