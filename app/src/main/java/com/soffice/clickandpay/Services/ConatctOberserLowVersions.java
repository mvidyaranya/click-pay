package com.soffice.clickandpay.Services;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;

import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;
import com.soffice.clickandpay.database.DataBaseManager;
import com.soffice.clickandpay.database.DataCenter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by sys2033 on 16/10/15.
 */
public class ConatctOberserLowVersions extends ContentObserver {
    public static boolean canSync = true;
    Cursor sms_sent_cursor;
    DataCenter smshelper;
    Cursor cur;
    ClickandPay clickpay;
    SessionManager session;
    String localContact_idlist;
    Cursor pCur1 = null, insertCursor = null;
    String mainversion, starred;
    long contact_Id, maxContactid = 0, maxContactid1 = 0;
    String sessionMaxID;
    ArrayList<String> deleteList, updateList, insertList;
    JSONObject localContactVersionslist;
    CountDownTimer timer = new CountDownTimer(5000, 1000) {

        public void onTick(long millisUntilFinished) {

        }

        public void onFinish() {
            Display.DisplayLogI("ADITYA", "onFinish Called");
            new ContactsRefreshTask().execute();

        }
    };
    private Context mContext;

    public ConatctOberserLowVersions(Handler handler, Context con) {
        super(handler);
        mContext = con;
        clickpay = (ClickandPay) mContext.getApplicationContext();
        smshelper = clickpay.getDatacenter();
    }

    @Override
    public void onChange(boolean selfChange) {

        Display.DisplayLogI("ADITYA", "CONTACTS LOWWWWWW");
        session = new SessionManager(mContext);//way2sms.getSession();

        if (canSync) {
            canSync = false;
            timer.start();
        }
        else {
            canSync = false;
            try {
                timer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
            timer.start();
        }

    }

    public void performSynchContacts() {

        SQLiteDatabase myDB = null;
        try {
            myDB = DataBaseManager.getInstance().openDatabase();
            if(session == null){
                session = clickpay.getSession();
            }
            localContact_idlist = session.getStoredContactlist();
            try {
                localContactVersionslist = new JSONObject(session.getStoredVersionsList());
            }catch (Exception e){
                localContactVersionslist = new JSONObject();
            }
            sessionMaxID = session.getMaxContact();
            ContentResolver cr = mContext.getContentResolver();
            pCur1 = cr.query(ContactsContract.RawContacts.CONTENT_URI, new String[]{ContactsContract.RawContacts.VERSION, ContactsContract.RawContacts.CONTACT_ID, ContactsContract.RawContacts.DELETED,ContactsContract.RawContacts.STARRED}, null, null, ContactsContract.RawContacts.CONTACT_ID + " ASC");
            int i = 1;
            JSONObject sessionContactlist = new JSONObject(), contact;
            ArrayList<Long> keyslist = new ArrayList<>();
            String Contact_idlist = "";
            Display.DisplayLogI("ADITYA", "Phone contacts Coursor count : " + pCur1.getCount());
            while (pCur1.moveToNext()) {
                contact_Id = pCur1.getLong(pCur1.getColumnIndex(ContactsContract.RawContacts.CONTACT_ID));
                mainversion = pCur1.getString(pCur1.getColumnIndex(ContactsContract.RawContacts.VERSION));
                starred = pCur1.getString(pCur1.getColumnIndex(ContactsContract.RawContacts.STARRED));
                /*if(contact_Id==4842){

                }*/
                Display.DisplayLogI("ADITYA", "MAIN contact_Id : " + contact_Id);
                Display.DisplayLogI("ADITYA", "MAIN starred : " + starred);
                Display.DisplayLogI("ADITYA", "MAIN VERSION : " + mainversion);
                if (keyslist.contains(contact_Id)) {

                    String version = sessionContactlist.get("" + contact_Id).toString();
                    version = version + "," + mainversion + " - " + starred;
                    sessionContactlist.put("" + contact_Id, version);

                } else {
                    if (contact_Id != 0) {
                        maxContactid = contact_Id;
                        keyslist.add(contact_Id);
                        sessionContactlist.put("" + contact_Id, "" + mainversion + " - " + starred);
                        if (Contact_idlist.length() == 0) {
                            Contact_idlist = "" + contact_Id;
                        } else {
                            Contact_idlist = Contact_idlist + "," + contact_Id;
                        }
                    }
                }
            }

            Display.DisplayLogI("ADITYA", "SESSION CONTACTS List : " + localContact_idlist);
            Display.DisplayLogI("ADITYA", "LOCAL CONTACTS List : " + Contact_idlist);
            Display.DisplayLogI("ADITYA", "LOCAL Version  List : " + sessionContactlist.toString());
            Display.DisplayLogI("ADITYA", "SESSION Version List : " + localContactVersionslist.toString());
            Display.DisplayLogI("ADITYA", "sessionMaxID : " + sessionMaxID);
            Display.DisplayLogI("ADITYA", "maxContactid : " + maxContactid);
            if (localContact_idlist != null && Contact_idlist != null && sessionContactlist != null && localContactVersionslist != null && !sessionContactlist.toString().equalsIgnoreCase(localContactVersionslist.toString())) {
//                if (localContactVersionslist != null && !localContactVersionslist.toString().equalsIgnoreCase("") && localContactVersionslist.toString() != "" && sessionContactlist != null && !sessionContactlist.toString().equalsIgnoreCase("") && sessionContactlist.toString() != "" && sessionContactlist.toString().equalsIgnoreCase(localContactVersionslist.toString())) {
//                }
//            } else {

                Display.DisplayLogI("ADITYA", "ELSE ELSE");
                deleteList = new ArrayList<>();
                updateList = new ArrayList<>();
                insertList = new ArrayList<>();
                String[] contactsIds = localContact_idlist.split(",");
                ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(Contact_idlist.split(",")));
                Display.DisplayLogI("ADITYA", "updatelist size" + updateList.size());
                for (int j = 0; j < contactsIds.length; j++) {
                    if (stringList.contains(contactsIds[j])) {
                        Display.DisplayLogI("ADITYA", "stringList.contains(contactsIds[j]) " + contactsIds[j]);
                        Display.DisplayLogI("ADITYA", "sessionContactlist.get(contactsIds[j]) != null && localContactVersionslist.get(contactsIds[j]) != null ");
                        if (!sessionContactlist.get(contactsIds[j]).toString().equalsIgnoreCase(localContactVersionslist.get(contactsIds[j]).toString())) {
                            Display.DisplayLogI("ADITYA", "sessionContactlist.get(contactsIds[j]).toString().equalsIgnoreCase(localContactVersionslist.get(contactsIds[j]).toString()) are not EQUAL ");
                            updateList.add(contactsIds[j]);
                        } else {
                            Display.DisplayLogI("ADITYA", "sessionContactlist.get(contactsIds[j]).toString().equalsIgnoreCase(localContactVersionslist.get(contactsIds[j]).toString()) ARE EQAL");
                        }

                    } else {
                        Display.DisplayLogI("ADITYA", "stringList.contains(contactsIds[j]) ELSE DELTE" + contactsIds[j]);
                        deleteList.add(contactsIds[j]);
                    }

                    if ((contactsIds.length - 1) > 0 && j == (contactsIds.length - 1)) {
                        maxContactid1 = Integer.parseInt(contactsIds[j]);
                    }
                }
                if (maxContactid > maxContactid1) {
                    insertCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + ">'" + maxContactid1 + "'",
                            null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE");
                    Display.DisplayLogI("ADITYA", "INSERt cursor size" + insertCursor.getCount());
                    if (insertCursor.getCount() > 0) {
                        smshelper.insertContactsNew(insertCursor, myDB);
                    }
                }
            }

            if (deleteList != null && deleteList.size() > 0) {
                for (String s : deleteList) {
                    Display.DisplayLogI("ADITYA CONTACTS NEW", "DELETE ID " + s);
                    smshelper.DeleteContact(s, myDB);
                }

            }
            if (updateList != null && updateList.size() > 0) {
                for (String s : updateList) {
                    insertCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "='" + s + "'",
                            null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE");
                    Display.DisplayLogI("ADITYA CONTACTS NEW", "UPDATE ID " + s);
                    smshelper.DeleteContact(s, myDB);
                    smshelper.insertContactsNew(insertCursor, myDB);
                }
                updateList = null;
            }

//            if (insertList != null && insertList.size() > 0) {
//                for (String s : insertList) {
//                    Display.DisplayLogI("ADITYA CONTACTS NEW", "INSERT ID " + s);
//                }
//            }

            session.setMaxContact("" + maxContactid);
            session.setStoredContactlist(Contact_idlist);
            session.setStoredVersionsList(sessionContactlist.toString());



//            if (minutes > 1) {
//                session.settimeDff(new Date().getTime());

//

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            myDB = null;
        }
    }

    @Override
    public boolean deliverSelfNotifications() {

        return super.deliverSelfNotifications();
    }

    private void sendResultMessage(Boolean data) {
        Intent intent = new Intent(Constants.TAB_FILTER);
        intent.putExtra(Constants.COMMAND, Constants.UPDATE_SMILEY);
        intent.putExtra(Constants.CONTACTS_REFRESH, data);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }


    public class ContactsRefreshTask extends AsyncTask<Void, Void, Void> {

        SQLiteDatabase myDB;
        Cursor curr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (DataBaseManager.getInstance() != null) {
                myDB = DataBaseManager.getInstance().openDatabase();

            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Display.DisplayLogI("Teja", "Its  working dropandInsertContacts");
                performSynchContacts();
                sendResultMessage(true);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (DataBaseManager.getInstance() != null) {
                DataBaseManager.getInstance().closeDatabase();
            }
            canSync = true;
            myDB = null;
        }
    }

}
