package com.soffice.clickandpay.Services;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;

import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.Pojo.smsfetch;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;
import com.soffice.clickandpay.database.DataCenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SmsReader extends BroadcastReceiver {
	public SmsReader() {
	}

	String[] ProcessMessage;
	String SenderMessage;
	Context context1;

	 DataCenter datahelper;

	public boolean isAlpha(String name) {
		return name.matches("[0-9+]+");
	}

	String SMSReceiver = "android.provider.Telephony.SMS_RECEIVED";
	String SMSDeliever = "android.provider.Telephony.SMS_DELIVER";
	String Vcode = "";
	// settings values
	boolean notificationstatus, popup_status, vibrate_status, wakeup_status;
	String vibrate_pattern, ringtonepath;

	ObserverService mService;
	MyServiceConnection mConn;
	boolean mIsBound;
	ClickandPay clickpay;

	class MyServiceConnection implements ServiceConnection {
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = ((ObserverService.LocalBinder) service).getService();
			mIsBound = true;
		}

		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mIsBound = false;
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		context1 = context;
		clickpay = (ClickandPay) context.getApplicationContext();
		SessionManager manger = clickpay.getSession();
		datahelper = clickpay.getDatacenter();
		mConn = new MyServiceConnection();

		if(!isMyServiceRunning(ObserverService.class)){
			Intent service = new Intent(context.getApplicationContext(), ObserverService.class);
			context.getApplicationContext().bindService(service, mConn, Context.BIND_AUTO_CREATE);
			context.getApplicationContext().startService(service);
		}

		Display.DisplayLogI("ADITYA", "SmsReader onReceive");

		/*if (intent.getAction().equalsIgnoreCase(SMSReceiver)) {
			try {
				Bundle bndl = intent.getExtras();
				SmsMessage[] msg = null;
				if (null != bndl) {
					Object[] pdus = (Object[]) bndl.get("pdus");
					msg = new SmsMessage[pdus.length];
					for (int i = 0; i < msg.length; i++) {
						msg[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);

						String sender = msg[i].getOriginatingAddress();
						String body = msg[i].getMessageBody();
						if (sender.contains("-WAYSMS") || sender.contains("-WAYAPP")) {
							if (body.contains("Your Way2SMS.com Account ")) {
								if (VerifyActivity.isVerifyLive) {
									body = body.replaceAll("Password: ", "Password:");
									String[] code = body.split("Password:");
									Vcode = code[1];
									if (!Vcode.contains("Your Way2SMS.com Account LoginId")) {
										Intent in = new Intent("SmsMessage.intent.MAIN").putExtra("get_msg", Vcode);
										Constant.VERIFICATION_CODE = code[1];
										context.sendBroadcast(in);
									}
								}
							} else if (body.contains("register @ way2")) {
								Display.DisplayLogI("ADITYA", "register");
								if (VerifyActivity.isVerifyLive) {
									Display.DisplayLogI("ADITYA", "isVerifyLive");
									VerifyActivity.edtVCO.setText("");
									body = body.replaceAll("Password: ","Password:");
									String[] code = body.split(" ");
									Vcode = code[0];
									Intent in = new Intent("SmsMessage.intent.MAIN").putExtra("get_msg", Vcode);
									Constant.VERIFICATION_CODE = code[0];
									context.sendBroadcast(in);
								}else{
									Display.DisplayLogI("ADITYA", "not isVerifyLive");
								}
							} else if (body.contains("change mobile number @ way2")) {
								if (VerifyActivity.isVerifyLive) {
									VerifyActivity.edtVCO.setText("");
									body = body.replaceAll("Password: ", "Password:");
									String[] code = body.split(" ");
									Vcode = code[0];
									Intent in = new Intent("SmsMessage.intent.MAIN").putExtra("get_msg", Vcode);
									Constant.VERIFICATION_CODE = code[0];
									context.sendBroadcast(in);
								}
							} else {
							}

						} else {
						}

					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	}

	private NotificationManager myNotificationManager;
	int conservations, nofmsgs = 0;
	ArrayList<smsfetch> totalmsgs;
	Intent resultIntent;
	Uri notification;
	String message, tomobilno, displayname = null;

	Ringtone r;

	public void ringtone() {

		try {
			if (ringtonepath.equals(null)) {notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			} else {
				notification = Uri.parse(ringtonepath);
			}
			r = RingtoneManager.getRingtone(context1, notification);
			r.play();
			new Handler().postDelayed(new Runnable() {

				// Using handler with postDelayed called runnable run method

				@Override
				public void run() {
					r.stop();
				}
			}, 2 * 1000); // wait for 2 seconds
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int randInt(int min, int max) {

		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public boolean isMyServiceRunning(String serviceClassName) {
		ActivityManager activityManager = (ActivityManager) context1
				.getApplicationContext().getSystemService(
						Context.ACTIVITY_SERVICE);
		List<RunningServiceInfo> services = activityManager
				.getRunningServices(Integer.MAX_VALUE);

		for (RunningServiceInfo runningServiceInfo : services) {
			if (runningServiceInfo.service.getClassName().equals(
					serviceClassName)) {
				return true;
			}
		}
		return false;
	}

	protected boolean isMyServiceRunning(Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context1.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
