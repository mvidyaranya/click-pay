package com.soffice.clickandpay.Utilty;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class SessionManager {
    private static final String PREF_NAME = "ClickAndPay";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String AUTH_KEY = "AUTH_KEY";
    public static final String WALLET_BAL = "WALLET_BAL";
    public static final String USER_NAME = "USER_NAME";
    public static final String EMAIL_ID = "EMAIL_ID";
    public static final String MOBILE_NUM = "MOBILE_NUM";
    public static final String IS_LOGGEDIN = "IS_LOGGEDIN";
    public static final String PROFILE = "PROFILE";
    public static final String Contacts = "Contacts";
    public static final String Versions = "Versions";
    public static final String MaxContacts = "MaxContacts";
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setDeviceId(String deviceId) {
        editor.putString(DEVICE_ID, deviceId);
        editor.commit();
    }

    public String getDeviceId() {
        return pref.getString(DEVICE_ID, null);
    }

    public void setAuthKey(String authKey) {
        editor.putString(AUTH_KEY, authKey);
        editor.commit();
    }

    public String getAuthKey() {
        return pref.getString(AUTH_KEY, null);
    }

    public void setWalletBal(String bal) {
        editor.putString(WALLET_BAL, bal);
        editor.commit();
    }

    public String getWalletBal() {
        return pref.getString(WALLET_BAL, null);
    }


    public void setEmailId(String email) {
        editor.putString(EMAIL_ID, email);
        editor.commit();
    }

    public String getEmailId() {
        return pref.getString(EMAIL_ID, null);
    }


    public void setMobileNum(String mobile) {
        editor.putString(MOBILE_NUM, mobile);
        editor.commit();
    }

    public String getMobileNum() {
        return pref.getString(MOBILE_NUM, null);
    }

    public void setIsLoggedIn(boolean loggedIn) {
        editor.putBoolean(IS_LOGGEDIN, loggedIn);
        editor.commit();
    }

    public Boolean getIsLoggedIn() {
        return pref.getBoolean(IS_LOGGEDIN, false);
    }

    public void setUserName(String loggedIn) {
        editor.putString(USER_NAME, loggedIn);
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(USER_NAME, "");
    }


    public void setProfile(String loggedIn) {
        editor.putString(PROFILE, loggedIn);
        editor.commit();
    }

    public String getProfile() {
        return pref.getString(PROFILE, "");
    }


    public String getStoredContactlist() {
        return pref.getString(Contacts, "");
    }

    public void setStoredContactlist(String b) {
        editor.putString(Contacts, b);
        editor.commit();
    }

    public String getStoredVersionsList() {
        return pref.getString(Versions, "");
    }

    public void setStoredVersionsList(String b) {
        editor.putString(Versions, b);

        editor.commit();

    }

    public String getMaxContact() {
        return pref.getString(MaxContacts, "");
    }

    public void setMaxContact(String b) {
        editor.putString(MaxContacts, b);
        editor.commit();
    }
}