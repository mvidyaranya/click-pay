package com.soffice.clickandpay.Utilty;

/**
 * Created by aditya on 11/2/16.
 */
public class Constants {

    public static String App_Version_Identifier = "V ";
    public static String App_Version = "1.0";
    public static final String SMS_REFRESH = "sms-refresh";
    public static final String CONTACTS_REFRESH = "contacts-refresh";
    public static final String SMS_COMPOSEREFRESH = "sms-composerefresh";
    public static final String TAB_FILTER = "Custom.DB.Refresh";
    public static final String UPDATE_SMILEY = "uPDATE.DB.Refresh";
    public static final String COMMAND = "COMMAND";
}
