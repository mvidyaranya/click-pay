package com.soffice.clickandpay.Utilty;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by sys2025 on 13/9/15.
 */
public class Display {
    Context context;

    public Display(Context cont) {
        context = cont;
    }

    public static void DisplayToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static void DisplayLogI(String tag , String msg) {
        Log.i(tag, msg);
    }

    public static void DisplayLogD(String tag , String msg) {
        Log.d(tag, msg);
    }

    public static void DisplayLogE(String tag , String msg) {
        Log.e(tag, msg);
    }
}
