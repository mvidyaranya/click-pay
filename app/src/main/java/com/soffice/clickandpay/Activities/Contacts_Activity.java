package com.soffice.clickandpay.Activities;

import android.app.LoaderManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.soffice.clickandpay.Adapters.ContactsCursorAdapter;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Services.ConatctOberserLowVersions;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;
import com.soffice.clickandpay.Utilty.SoftKeyboardStateHelper;
import com.soffice.clickandpay.database.DataBaseManager;
import com.soffice.clickandpay.database.DataCenter;

public class Contacts_Activity extends AppCompatActivity implements AdapterView.OnItemClickListener,
        SoftKeyboardStateHelper.SoftKeyboardStateListener, LoaderManager.LoaderCallbacks<Cursor> {

    SQLiteDatabase mYDB;
    DataCenter datacenter;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    String className, methodName, smsads, MID;
    Context context;
    String Message;
    RelativeLayout AdsLayout;
    ListView ContactsListView;
    ContactsCursorAdapter adapter;
    int adposition = 3;
    Cursor cur = null, pCur;
    EditText SearchTxt;
    ImageView imageicon;
    RelativeLayout edtLl;
    private String mCurFilter;
    RelativeLayout relSearch;
    TextView noContacts;
    public static int w, h, wm, hm;
    RelativeLayout Enter_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_contacts_);
            try {
                getSupportActionBar().hide();
            } catch (Exception e) {
                e.printStackTrace();
            }
            context = this;
            clickpay = (ClickandPay) getApplicationContext();
            datacenter = clickpay.getDatacenter();
            mYDB = DataBaseManager.getInstance().openDatabase();
            session = clickpay.getSession();
            className = getLocalClassName();

            createViews();


            relSearch.setVisibility(View.VISIBLE);
            ContactsListView.setFastScrollEnabled(true);
            ContactsListView.setOnItemClickListener(this);
            ContactsListView.setTextFilterEnabled(true);
            ContactsListView.setFastScrollEnabled(true);
            ContactsListView.setFastScrollEnabled(true);
            ContactsListView.setOnItemClickListener(this);

            if (getIntent() != null && getIntent().getExtras() != null && getIntent().getStringExtra("MESSAGE") != null) {
                Message = getIntent().getStringExtra("MESSAGE");
            }


                adapter = new ContactsCursorAdapter(context, cur, false, adposition, "Sticky");

            ContactsListView.setAdapter(adapter);
            // getLoaderManager().initLoader(0, null, Contacts_Activity.this);
            getLoaderManager().initLoader(0, null, this);

            SearchTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SearchTxt.setCursorVisible(true);
                }
            });

            imageicon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (SearchTxt.getText().toString().length() > 0) {
                        SearchTxt.setText("");
                        imageicon.setVisibility(View.GONE);
                        Enter_message.setVisibility(View.GONE);
                        edtLl.setVisibility(View.VISIBLE);
                        SearchTxt.clearFocus();
                        removePhoneKeypad();
                        SearchTxt.setHint("Search contact number or name");
                    }
                }
            });

            SearchTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        SearchTxt.setHint("");
                        imageicon.setVisibility(View.GONE);
                    } else {
                        imageicon.performClick();
                        SearchTxt.setCursorVisible(false);
                    }

                }
            });

            SearchTxt.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                    if (charSequence.length() == 0
                            || charSequence.length() < 10) {
                        ContactsListView.setSelection(0);
                    }
                    if (charSequence.length() > 0) {

                        imageicon.setImageResource(R.mipmap.close_payment_details);
                        edtLl.setBackgroundResource(R.drawable.edt_bg_colored);

                    } else {
                        imageicon.setVisibility(View.GONE);
                        edtLl.setBackgroundResource(R.drawable.edt_bg_dull);
                    }
                    mCurFilter = TextUtils.isEmpty(charSequence) ? null
                            : charSequence.toString();
                    getLoaderManager().restartLoader(0, null,
                            Contacts_Activity.this);
                    if (charSequence.length() >= 10
                            && !charSequence.toString().substring(0, 1)
                            .matches("[0-6]")
                            && adapter.getCount() == 0
                            && charSequence.toString().matches("[0-9]+")) {
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() == 0) {
                        imageicon.setVisibility(View.GONE);
                    } else {
                        imageicon.setVisibility(View.VISIBLE);
                    }
                }
            });

            Enter_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String Phoneno = null;

                    try {
                        /*Intent i = new Intent(Contacts_Activity.this, ComposeActivity.class);
                        Phoneno = SearchTxt.getText().toString();
                        if (Phoneno.length() != 0) {
                            if (Phoneno.length() == 10) {
                                if (Phoneno.substring(0, 1).matches("[7-9]")) {
                                    Phoneno = SearchTxt.getText().toString();
                                } else {
                                    Phoneno = null;
                                }
                            } else {
                                Phoneno = null;
                            }
                        } else {
                            Phoneno = null;
                        }

                        try {
                            i.putExtra("NUMBER", Phoneno);
                            i.putExtra("FROM", "FAV");
                            i.putExtra("FROM", "FORWARD");
                            i.putExtra("MESSAGE", Message);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (Phoneno != null) {
                            imageicon.setVisibility(View.GONE);
                            SearchTxt.setText("");
                            SearchTxt.setHint("Type mobile number");
                            SearchTxt.setBackgroundColor(Color.WHITE);
                            SearchTxt.setTextColor(Color.BLACK);

                            SearchTxt.setCursorVisible(true);

                            startActivity(i);

                            finish();
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        Phoneno = null;
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createViews() {
        ContactsListView = (ListView) findViewById(R.id.contactslistview);
        SearchTxt = (EditText) findViewById(R.id.SearchTxt);
        imageicon = (ImageView) findViewById(R.id.edt_img);
        edtLl = (RelativeLayout) findViewById(R.id.edt_lay);
        noContacts = (TextView) findViewById(R.id.noContacts);
        relSearch = (RelativeLayout) findViewById(R.id.rel_search);
        Enter_message = (RelativeLayout) findViewById(R.id.Enter_message);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (DataBaseManager.getInstance() == null) {
            DataBaseManager.getInstance().openDatabase();
        }else{
            if(datacenter == null){
                datacenter = clickpay.getDatacenter();
            }
            DataBaseManager.initializeInstance(datacenter);
            DataBaseManager.getInstance().openDatabase();
        }
    }

    @Override
    public void onResume() {

        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (datacenter != null ) {
                        if (datacenter.getContacts(mYDB) == null || datacenter.getContacts(mYDB).getCount() == 0 ) {
                            ConatctOberserLowVersions observer = new ConatctOberserLowVersions(new Handler(), getBaseContext());
                            session.setStoredContactlist("");
                            session.setStoredVersionsList("");
                            observer.new ContactsRefreshTask().execute();
                            Display.DisplayLogI("ADITYA", "CONTACTS ZERO");
                        }
                    }
                }
            }, 2000);
        } catch (Exception e) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    DataBaseManager.getInstance().openDatabase();
                    if (datacenter != null ) {
                        if (datacenter.getContacts(mYDB) == null || datacenter.getContacts(mYDB).getCount() == 0 ) {
                            ConatctOberserLowVersions observer = new ConatctOberserLowVersions(new Handler(), getBaseContext());
                            session.setStoredContactlist("");
                            session.setStoredVersionsList("");
                            observer.new ContactsRefreshTask().execute();
                            Display.DisplayLogI("ADITYA", "CONTACTS ZERO");
                        }
                    }
                }
            }, 2000);
        }

        try {
            if (ContactsListView != null) {
                ContactsListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
                ContactsListView.setSelection(0);
                ContactsListView.setItemChecked(0, true);
                ContactsListView.invalidateViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
                return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void removePhoneKeypad() {
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            removePhoneKeypad();
            /*Cursor cur = (Cursor) ContactsListView.getItemAtPosition(position);
            Intent sms = new Intent(context, ComposeActivity.class);
            sms.putExtra("FROM", "FORWARD");
            sms.putExtra("NAME", cur.getString(cur.getColumnIndex("name")));
            sms.putExtra("NUMBER", cur.getString(cur.getColumnIndex("number")));
            sms.putExtra("MESSAGE", Message);
            startActivity(sms);
            finish();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeightInPx) {
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    ContactsListView.setFastScrollEnabled(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onSoftKeyboardClosed() {
        try {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    ContactsListView.setFastScrollEnabled(true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        try {
            if (mCurFilter != null) {
                return new CursorLoader(context, null, null, null, null, null) {
                    @Override
                    public Cursor loadInBackground() {
                        return datacenter.getlikecontactdata(mCurFilter, mYDB);
                    }
                };
            } else {
                return new CursorLoader(context, null, null, null, null, null) {
                    @Override
                    public Cursor loadInBackground() {
                        return datacenter.getContacts(mYDB);
                    }
                };
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
        // adapter.swapCursor(cursor);
        // adapter.notifyDataSetChanged();
        if (cursor.getCount() > 0) {
            noContacts.setVisibility(View.GONE);
            adapter = new ContactsCursorAdapter(context, cursor, false, adposition, "Sticky");
            ContactsListView.clearChoices();
            ContactsListView.setAdapter(adapter);

            ContactsListView.post(new Runnable() {
                @Override
                public void run() {
                    ContactsListView.invalidateViews();

                }
            });
        } else {
            noContacts.setVisibility(View.VISIBLE);
            if (SearchTxt.getText().length() >= 10
                    && !SearchTxt.getText().toString().substring(0, 1)
                    .matches("[0-6]") && cursor.getCount() == 0
                    && SearchTxt.getText().toString().matches("[0-9]+")) {

                Enter_message.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        adapter.changeCursor(null);

    }



    /************
     * INSERT SMS AND CONTACTS INTO DATABASE
     ************/

    class InsertsmsData extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ContentResolver cr = getContentResolver();
            try {
                pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID, null,
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE NOCASE");
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    if (datacenter.insertContacts(pCur, mYDB)) {
                        Display.DisplayLogI("ADITYA", "Inserting Data Lower");
                    }
                } else {
                    if (datacenter.insertContacts(pCur, mYDB)) {
                        Display.DisplayLogI("ADITYA", "Inserting Data");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (pCur != null) {
                    pCur.close();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                Display.DisplayLogI("ADITYA", "SMS AND CONTACTS DUMP STARTED");
                DataBaseManager.getInstance().openDatabase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                Cursor c = datacenter.getContacts(mYDB);
                Display.DisplayLogI("ADITYA", "COUNT CONTACTS DUMP " + c.getCount());
                DataBaseManager.getInstance().closeDatabase();
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(result);
            Display.DisplayLogI("ADITYA", "SMS AND CONTACTS DUMP DONE");
        }

    }

}
