package com.soffice.clickandpay.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.RegistrationResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements TaskListner {

    EditText name_EditText, email_EditText, password_EditText, mobile_EditText, refferal_EditText;
    CardView getStartedLayout;
    TextView signIn_TV;
    static TextView dob_EditText;
    ImageView back_IV;
    String name, email, passwd, mobile, dob, refferal;
    JsonRequester requester;
    String className;
    String methodName;
    Urls urls;
    SessionManager session;
    ClickandPay clickpay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        className = getLocalClassName();
        requester = new JsonRequester(this);
        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();

        urls = clickpay.getUrls();

        name_EditText = (EditText) findViewById(R.id.name_EditText);
        email_EditText = (EditText) findViewById(R.id.email_EditText);
        password_EditText = (EditText) findViewById(R.id.password_EditText);
        mobile_EditText = (EditText) findViewById(R.id.mobile_EditText);
        dob_EditText = (TextView) findViewById(R.id.dob_EditText);
        refferal_EditText = (EditText) findViewById(R.id.refferal_EditText);
        getStartedLayout = (CardView) findViewById(R.id.getStartedLayout);
        signIn_TV = (TextView) findViewById(R.id.signIn_TV);
        back_IV = (ImageView) findViewById(R.id.back_IV);

        getStartedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = name_EditText.getText().toString();
                email = email_EditText.getText().toString();
                passwd = password_EditText.getText().toString();
                mobile = mobile_EditText.getText().toString();
                dob = dob_EditText.getText().toString();
                refferal = refferal_EditText.getText().toString();

                if (name != null && name.length() > 2) {
                    if (email != null && email.length() > 2) {
                        if (email != null && isEmailValid(email)) {
                            if (passwd != null && passwd.length() > 2) {
                                if (mobile != null && mobile.length() == 10) {
                                    if (mobile.charAt(0) == '7' || mobile.charAt(0) == '8' || mobile.charAt(0) == '9') {
                                        if (dob != null && dob.length() > 2) {
                                            Map<String, String> params = new HashMap<String, String>();
                                            params.put("version", Constants.App_Version);
                                            params.put("userName", name);
                                            params.put("DOB", dob);
                                            params.put("emailId", email);
                                            params.put("mobile", mobile);
                                            params.put("password", passwd);
                                            params.put("refCode", refferal);
                                            params.put("mid", clickpay.getDeviceId(getApplicationContext()));
                                            methodName = urls.userSignUp_methodName;
                                            requester.StringRequesterFormValues(urls.userSignUp, Request.Method.POST, className, methodName, params);
                                        } else {
                                            Display.DisplayToast(RegisterActivity.this, "Please enter your Date of Birth");
                                        }
                                    } else {
                                        Display.DisplayToast(RegisterActivity.this, "Please enter valid mobile number starts with 7 / 8 / 9");
                                    }
                                } else {
                                    Display.DisplayToast(RegisterActivity.this, "Please enter your Mobile Number");
                                }
                            } else {
                                Display.DisplayToast(RegisterActivity.this, "Please enter your Password");
                            }
                        } else {
                            Display.DisplayToast(RegisterActivity.this, "Please enter valid Email");
                        }
                    } else {
                        Display.DisplayToast(RegisterActivity.this, "Please enter your Email");
                    }
                } else {
                    Display.DisplayToast(RegisterActivity.this, "Please enter your Full Name");
                }
            }
        });

        signIn_TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dob_EditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getSupportFragmentManager(), "DatePicker");
            }
        });

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Initial")) {
                    Intent i = new Intent(RegisterActivity.this, InitialActivity.class);
                    i.putExtra("fromActivity", "Register");
                    startActivity(i);
                    finish();
                } else {
                    finish();
                }
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    public void removePhoneKeypad() {

        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Initial")) {
            Intent i = new Intent(RegisterActivity.this, InitialActivity.class);
            i.putExtra("fromActivity", "Register");
            startActivity(i);
            finish();
        } else {
            finish();
        }
    }

    @SuppressLint("ValidFragment")
    public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar calendar = Calendar.getInstance();
            int yy = calendar.get(Calendar.YEAR);
            int mm = calendar.get(Calendar.MONTH);
            int dd = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, yy, mm, dd);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }
    }

    public void populateSetDate(int year, int month, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date strDate = null;
        try {
            strDate = sdf.parse(day + "/" + month + "/" + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Display.DisplayLogI("ADITYA", "ERRE " + new Date().after(strDate));
        if (new Date().after(strDate)) {
            dob_EditText.setText(day + "/" + month + "/" + year);
        } else {
            Display.DisplayLogI("ADITYA", "ERRE");
            Display.DisplayToast(getApplicationContext(), "Date of Birth can not be a future date");
            dob_EditText.setText("");
        }
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.userSignUp_methodName)) {
                Gson g = new Gson();
                RegistrationResponseModel model = g.fromJson(response, RegistrationResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    Display.DisplayToast(RegisterActivity.this, "OTP : " + model.OTP);
                    session.setAuthKey(model.key);
                    session.setEmailId(email);
                    session.setMobileNum(mobile);

                    Intent i = new Intent(RegisterActivity.this, VerificationActivity.class);
                    i.putExtra("VERIFY_CODE", model.OTP);
                    i.putExtra("fromActivity", "Register");
                    startActivity(i);
                    finish();
                } else {
                    Display.DisplayToast(RegisterActivity.this, model.message);
                }
            }
        }
    }
}
