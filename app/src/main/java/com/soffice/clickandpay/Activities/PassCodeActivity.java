package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.GetProfileResonseModel;
import com.soffice.clickandpay.Pojo.SetPassCodeResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class PassCodeActivity extends AppCompatActivity implements TaskListner {

    String className;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    ImageView passCode1, passCode2, passCode3, passCode4, backSpace,back_IV;
    TextView key1, key2, key3, key4, key5, key6, key7, key8, key9, key0,tv1;
    static String enteredCode = "";
    static String conformEnteredCode = "";
    View.OnClickListener clickEventOnKeys;
    View.OnClickListener clickEventOnKeysConform;
    View.OnClickListener backspaceEvent;
    View.OnClickListener conformBackspaceEvent;
    static String fromActivity;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();
        fromActivity = getIntent().getStringExtra("fromActivity");

        passCode1 = (ImageView) findViewById(R.id.passcode1);
        passCode2 = (ImageView) findViewById(R.id.passcode2);
        passCode3 = (ImageView) findViewById(R.id.passcode3);
        passCode4 = (ImageView) findViewById(R.id.passcode4);
        backSpace = (ImageView) findViewById(R.id.backSpace);
        back_IV = (ImageView) findViewById(R.id.back_IV);
        tv1 = (TextView) findViewById(R.id.tv1);

        key1 = (TextView) findViewById(R.id.key1);
        key2 = (TextView) findViewById(R.id.key2);
        key3 = (TextView) findViewById(R.id.key3);
        key4 = (TextView) findViewById(R.id.key4);
        key5 = (TextView) findViewById(R.id.key5);
        key6 = (TextView) findViewById(R.id.key6);
        key7 = (TextView) findViewById(R.id.key7);
        key8 = (TextView) findViewById(R.id.key8);
        key9 = (TextView) findViewById(R.id.key9);
        key0 = (TextView) findViewById(R.id.key0);

        if(fromActivity.equalsIgnoreCase("Login")){
            tv1.setText("Enter Passcode");
            back_IV.setVisibility(View.GONE);
        }

        clickEventOnKeys = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enteredCode = ""+enteredCode+v.getTag();
                Display.DisplayLogI("ADITYA", "CLICK PASSCODE " + enteredCode);
                if(enteredCode.length() == 1){
                    passCode1.setImageResource(R.mipmap.passcode_colored2x);
                }else if(enteredCode.length() == 2){
                    passCode2.setImageResource(R.mipmap.passcode_colored2x);
                }else if(enteredCode.length() == 3){
                    passCode3.setImageResource(R.mipmap.passcode_colored2x);
                }else if(enteredCode.length() == 4){
                    passCode4.setImageResource(R.mipmap.passcode_colored2x);
                    disableKeys();
                    if(fromActivity.equalsIgnoreCase("Verification")) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                prepareForConformPasscode();
                            }
                        }, 500);
                    }else if(fromActivity.equalsIgnoreCase("Login")) {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("authkey", session.getAuthKey());
                        params.put("passcode", enteredCode);
                        requester.StringRequesterFormValues(urls.check_passcode, Request.Method.POST, className, urls.check_passcode_methodName, params);
                    }
                    Display.DisplayLogI("ADITYA", "CLICK PASSCODE ");
                }
            }
        };

        clickEventOnKeysConform = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conformEnteredCode = ""+conformEnteredCode+v.getTag();
                Display.DisplayLogI("ADITYA", "CLICK CONFORM PASSCODE " + conformEnteredCode);
                if(conformEnteredCode.length() == 1){
                    passCode1.setImageResource(R.mipmap.passcode_colored2x);
                }else if(conformEnteredCode.length() == 2){
                    passCode2.setImageResource(R.mipmap.passcode_colored2x);
                }else if(conformEnteredCode.length() == 3){
                    passCode3.setImageResource(R.mipmap.passcode_colored2x);
                }else if(conformEnteredCode.length() == 4){
                    passCode4.setImageResource(R.mipmap.passcode_colored2x);
                    disableKeys();
                    if(enteredCode.equalsIgnoreCase(conformEnteredCode)){
                        if (session.getAuthKey() != null && session.getAuthKey().length() > 2) {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("authkey", session.getAuthKey());
                            params.put("mid", clickpay.getDeviceId(getApplicationContext()));
                            params.put("passcode", enteredCode);
                            params.put("version", Constants.App_Version);
                            requester.StringRequesterFormValues(urls.passcode, Request.Method.POST, className, urls.passcode_methodName, params);
                        }
                    }else{
                        enteredCode = "";
                        conformEnteredCode = "";
                        prepareForPasscode();
                        Display.DisplayToast(PassCodeActivity.this, "Pass Code does not match..!!");
                    }
                }
            }
        };

        backspaceEvent = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enteredCode = removeLastChar(enteredCode);
                Display.DisplayLogI("ADITYA", "CLICK BACKSPACE " + enteredCode);
                if(enteredCode.length() < 4){
                    enableKeys();
                }
                if(enteredCode.length() == 1){
                    passCode2.setImageResource(R.mipmap.passcode2x);
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(enteredCode.length() == 2){
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(enteredCode.length() == 3){
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(enteredCode.length() == 0){
                    passCode1.setImageResource(R.mipmap.passcode2x);
                    passCode2.setImageResource(R.mipmap.passcode2x);
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }
            }
        };

        conformBackspaceEvent = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conformEnteredCode = removeLastChar(conformEnteredCode);
                if(conformEnteredCode.length() < 4){
                    enableKeys();
                }
                if(conformEnteredCode.length() == 1){
                    passCode2.setImageResource(R.mipmap.passcode2x);
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(conformEnteredCode.length() == 2){
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(conformEnteredCode.length() == 3){
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }else if(conformEnteredCode.length() == 0){
                    passCode1.setImageResource(R.mipmap.passcode2x);
                    passCode2.setImageResource(R.mipmap.passcode2x);
                    passCode3.setImageResource(R.mipmap.passcode2x);
                    passCode4.setImageResource(R.mipmap.passcode2x);
                }
            }
        };

        backSpace.setOnClickListener(backspaceEvent);

        key1.setOnClickListener(clickEventOnKeys);
        key2.setOnClickListener(clickEventOnKeys);
        key3.setOnClickListener(clickEventOnKeys);
        key4.setOnClickListener(clickEventOnKeys);
        key5.setOnClickListener(clickEventOnKeys);
        key6.setOnClickListener(clickEventOnKeys);
        key7.setOnClickListener(clickEventOnKeys);
        key8.setOnClickListener(clickEventOnKeys);
        key9.setOnClickListener(clickEventOnKeys);
        key0.setOnClickListener(clickEventOnKeys);

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Verification")) {
                    Intent i = new Intent(PassCodeActivity.this, InitialActivity.class);
                    i.putExtra("fromActivity", "PassCode");
                    startActivity(i);
                    finish();
                }else{
                    finish();
                }
                enteredCode = "";
            }
        });

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    public void prepareForConformPasscode(){

        Display.DisplayLogI("ADITYA", "CLICK prepareForConformPasscode ");

        tv1.setText("Re-Enter New Passcode");

        passCode1.setImageResource(R.mipmap.passcode2x);
        passCode2.setImageResource(R.mipmap.passcode2x);
        passCode3.setImageResource(R.mipmap.passcode2x);
        passCode4.setImageResource(R.mipmap.passcode2x);

        key1.setOnClickListener(clickEventOnKeysConform);
        key2.setOnClickListener(clickEventOnKeysConform);
        key3.setOnClickListener(clickEventOnKeysConform);
        key4.setOnClickListener(clickEventOnKeysConform);
        key5.setOnClickListener(clickEventOnKeysConform);
        key6.setOnClickListener(clickEventOnKeysConform);
        key7.setOnClickListener(clickEventOnKeysConform);
        key8.setOnClickListener(clickEventOnKeysConform);
        key9.setOnClickListener(clickEventOnKeysConform);
        key0.setOnClickListener(clickEventOnKeysConform);

        backSpace.setOnClickListener(conformBackspaceEvent);

        enableKeys();
    }

    public void prepareForPasscode(){

        Display.DisplayLogI("ADITYA", "CLICK prepareForPasscode ");

        passCode1.setImageResource(R.mipmap.passcode2x);
        passCode2.setImageResource(R.mipmap.passcode2x);
        passCode3.setImageResource(R.mipmap.passcode2x);
        passCode4.setImageResource(R.mipmap.passcode2x);

        key1.setOnClickListener(clickEventOnKeys);
        key2.setOnClickListener(clickEventOnKeys);
        key3.setOnClickListener(clickEventOnKeys);
        key4.setOnClickListener(clickEventOnKeys);
        key5.setOnClickListener(clickEventOnKeys);
        key6.setOnClickListener(clickEventOnKeys);
        key7.setOnClickListener(clickEventOnKeys);
        key8.setOnClickListener(clickEventOnKeys);
        key9.setOnClickListener(clickEventOnKeys);
        key0.setOnClickListener(clickEventOnKeys);

        backSpace.setOnClickListener(backspaceEvent);

        enableKeys();
    }

    public void enableKeys(){
        key1.setEnabled(true);
        key2.setEnabled(true);
        key3.setEnabled(true);
        key4.setEnabled(true);
        key5.setEnabled(true);
        key6.setEnabled(true);
        key7.setEnabled(true);
        key8.setEnabled(true);
        key9.setEnabled(true);
        key0.setEnabled(true);
    }

    public void disableKeys(){
        /*key1.setEnabled(false);
        key2.setEnabled(false);
        key3.setEnabled(false);
        key4.setEnabled(false);
        key5.setEnabled(false);
        key6.setEnabled(false);
        key7.setEnabled(false);
        key8.setEnabled(false);
        key9.setEnabled(false);
        key0.setEnabled(false);*/
    }

    public String removeLastChar(String str) {
        if (str.length() > 0) {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }

    @Override
    protected void onPause() {
        super.onPause();
        enteredCode = "";
    }

    public void getProfileData(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("authkey", session.getAuthKey());
        requester.StringRequesterFormValues(urls.getProfile, Request.Method.POST, className, urls.getProfile_methodName, params);
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.passcode_methodName)) {
                Gson g = new Gson();
                SetPassCodeResponseModel model = g.fromJson(response, SetPassCodeResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setWalletBal(model.wallet_amount);
                    Intent i = new Intent(PassCodeActivity.this, MainActivity.class);
                    startActivity(i);
                    getProfileData();
                    finish();
                }else{
                    Display.DisplayToast(PassCodeActivity.this, model.message);
                }
            }

            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.check_passcode_methodName)) {
                Gson g = new Gson();
                SetPassCodeResponseModel model = g.fromJson(response, SetPassCodeResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setWalletBal(model.wallet_amount);
                    Intent i = new Intent(PassCodeActivity.this, MainActivity.class);
                    startActivity(i);
                    getProfileData();
                    finish();
                }else{
                    Display.DisplayToast(PassCodeActivity.this, model.message);
                }
            }

            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.getProfile_methodName)) {
                Gson g = new Gson();
                GetProfileResonseModel model = g.fromJson(response, GetProfileResonseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    Display.DisplayLogI("ADITYA", "GET PROFILE : " + g.toJson(model.profile.get(0)));
                    session.setProfile(g.toJson(model.profile.get(0)));
                    session.setUserName(model.profile.get(0).userName);
                }else{
                    Display.DisplayToast(PassCodeActivity.this, model.message);
                }
            }
        }
    }
}
