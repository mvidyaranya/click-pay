package com.soffice.clickandpay.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

public class SuggestAStireActivity extends AppCompatActivity implements TaskListner {

    EditText storeName_ET, locality_ET, city_ET, additional_info_ET;
    String ss = "<font color=#000000>";
    String ss1 = "Code : ";
    String ss2 = "</font> ";
    String ss8 = "<font color=#bf0e14>";
    String ss4 = "</font>";
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    String className;
    ImageView back_IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_astire);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();

        storeName_ET = (EditText) findViewById(R.id.storeName_ET);
        locality_ET = (EditText) findViewById(R.id.locality_ET);
        city_ET = (EditText) findViewById(R.id.city_ET);
        additional_info_ET = (EditText) findViewById(R.id.additional_info_ET);
        back_IV = (ImageView) findViewById(R.id.back_IV);

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        storeName_ET.setHint(Html.fromHtml("Store Name"+ss8+"* "+ss4));
        locality_ET.setHint(Html.fromHtml("Locality"+ss8+"* "+ss4));
        city_ET.setHint(Html.fromHtml("City"+ss8+"* "+ss4));

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            /*if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.otpVerification_methodName)) {
                Gson g = new Gson();
                VerificationResponseModel model = g.fromJson(response, VerificationResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setAuthKey(model.key);

                    Intent i = new Intent(VerificationActivity.this, PassCodeActivity.class);
                    i.putExtra("fromActivity", "Verification");
                    startActivity(i);
                    finish();
                } else {
                    Display.DisplayToast(VerificationActivity.this, "Error : " + response);
                }
            }*/
        }
    }
}
