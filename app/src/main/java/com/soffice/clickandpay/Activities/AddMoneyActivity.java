package com.soffice.clickandpay.Activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.soffice.clickandpay.Adapters.CardsFragmentPagerAdapter;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.UI.PagerSlidingTabStrip;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

public class AddMoneyActivity extends AppCompatActivity implements TaskListner {

    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    String className;
    ViewPager viewPager;
    PagerSlidingTabStrip tabsStrip;
    CardsFragmentPagerAdapter cardsAdp;
    TextView pay_rupees, walletBal_Home;
    ImageView back_IV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();
        className = getLocalClassName();

        pay_rupees = (TextView) findViewById(R.id.pay_rupees);
        back_IV = (ImageView) findViewById(R.id.back_IV);
        walletBal_Home = (TextView) findViewById(R.id.walletBal_Home);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager_addMoney);
        cardsAdp = new CardsFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(cardsAdp);

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);

        if(session.getWalletBal().equalsIgnoreCase("0") || !session.getWalletBal().contains(".")) {
            walletBal_Home.setText(session.getWalletBal()+".00");
        }

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.showPassCode = false;
                finish();
            }
        });

        pay_rupees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.showPassCode = false;
        finish();
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.payment_methodName)) {
               /* Gson g = new Gson();
                PaymentResponseModel model = g.fromJson(response, PaymentResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setWalletBal(model.wallet_bal);
                    Intent i = new Intent(PaymentDetailsActivity.this, PaymentSuccessActivity.class);
                    i.putExtra("fromActivity", "PaymentDetails");
                    i.putExtra("vendorCode", vendorCode);
                    i.putExtra("amount", payingAmount);
                    startActivity(i);
                    finish();
                } else {
                    Display.DisplayToast(this, "Error : " + response);
                }*/
            }
        }
    }
}
