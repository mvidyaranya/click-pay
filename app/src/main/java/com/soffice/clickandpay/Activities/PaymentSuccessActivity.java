package com.soffice.clickandpay.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.FeedbackResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class PaymentSuccessActivity extends AppCompatActivity implements TaskListner {

    TextView codeEntered, payment_amount_label, submit_experience_tv, submit_feedback_tv, done_button,
            cancel_experience_tv, cancel_feedback_tv;
    LinearLayout smilei1_layout, smilei2_layout, smilei3_layout, smilei4_layout;
    RelativeLayout experienceLayout, feedLayout, sayDoneLayout;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    String className;
    EditText share_experience_EditText, share_feedback_EditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();

        codeEntered = (TextView) findViewById(R.id.codeEntered);
        payment_amount_label = (TextView) findViewById(R.id.payment_amount_label);
        smilei1_layout = (LinearLayout) findViewById(R.id.smilei1_layout);
        smilei2_layout = (LinearLayout) findViewById(R.id.smilei2_layout);
        smilei3_layout = (LinearLayout) findViewById(R.id.smilei3_layout);
        smilei4_layout = (LinearLayout) findViewById(R.id.smilei4_layout);

        experienceLayout = (RelativeLayout) findViewById(R.id.experienceLayout);
        feedLayout = (RelativeLayout) findViewById(R.id.feedLayout);
        sayDoneLayout = (RelativeLayout) findViewById(R.id.sayDoneLayout);

        share_experience_EditText = (EditText) findViewById(R.id.share_experience_EditText);
        share_feedback_EditText = (EditText) findViewById(R.id.share_feedback_EditText);
        submit_experience_tv = (TextView) findViewById(R.id.submit_experience_tv);
        submit_feedback_tv = (TextView) findViewById(R.id.submit_feedback_tv);
        done_button = (TextView) findViewById(R.id.done_button);
        cancel_experience_tv = (TextView) findViewById(R.id.cancel_experience_tv);
        cancel_feedback_tv = (TextView) findViewById(R.id.cancel_feedback_tv);

        experienceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        feedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        codeEntered.setText("Code : " + getIntent().getStringExtra("vendorCode"));
        payment_amount_label.setText("\u20B9 " + getIntent().getStringExtra("amount"));

        smilei1_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedLayout.setVisibility(View.VISIBLE);
            }
        });

        smilei2_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedLayout.setVisibility(View.VISIBLE);
            }
        });

        smilei3_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                experienceLayout.setVisibility(View.VISIBLE);
            }
        });

        smilei4_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                experienceLayout.setVisibility(View.VISIBLE);
            }
        });

        submit_experience_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (share_experience_EditText.getText().toString().length() > 1) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("authkey", session.getAuthKey());
                    params.put("tid", clickpay.getDeviceId(getApplicationContext()));
                    params.put("rating", "5");
                    params.put("comments", share_experience_EditText.getText().toString());
                    requester.StringRequesterFormValues(urls.feedback, Request.Method.POST, className, urls.feedback_methodName, params);
                }else{
                    Display.DisplayToast(PaymentSuccessActivity.this, "Please write your experience to improve better to best.. :)");
                }
            }
        });

        submit_feedback_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (share_feedback_EditText.getText().toString().length() > 1) {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("authkey", session.getAuthKey());
                    params.put("tid", clickpay.getDeviceId(getApplicationContext()));
                    params.put("rating", "1");
                    params.put("comments", share_feedback_EditText.getText().toString());
                    requester.StringRequesterFormValues(urls.feedback, Request.Method.POST, className, urls.feedback_methodName, params);
                }else{
                    Display.DisplayToast(PaymentSuccessActivity.this, "Please write your experience to improve better to best.. :)");
                }
            }
        });

        cancel_experience_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancel_feedback_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        done_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.feedback_methodName)) {
                Gson g = new Gson();
                FeedbackResponseModel model = g.fromJson(response, FeedbackResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    experienceLayout.setVisibility(View.GONE);
                    feedLayout.setVisibility(View.GONE);
                    sayDoneLayout.setVisibility(View.VISIBLE);
                } else {
                    Display.DisplayToast(PaymentSuccessActivity.this, "Error : " + response);
                }
            }
        }
    }
}
