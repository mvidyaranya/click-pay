package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.Adapters.TranscationListAdapter;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.SignInResponseModel;
import com.soffice.clickandpay.Pojo.TransactionItem;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionsActivity extends AppCompatActivity implements TaskListner {

    private List<TransactionItem> feedItemList = new ArrayList<TransactionItem>();
    private RecyclerView mRecyclerView;
    private TranscationListAdapter adapter;
    ClickandPay clickPay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    String className;
    ImageView back_IV,transactions_filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickPay = (ClickandPay) getApplicationContext();
        session = clickPay.getSession();
        session.setIsLoggedIn(true);
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickPay.getUrls();

        Map<String, String> params = new HashMap<String, String>();
        params.put("authkey", session.getAuthKey() );
        requester.StringRequesterFormValues(urls.getTransactions, Request.Method.POST, className, urls.getTransactions_methodName, params);

        transactions_filter = (ImageView) findViewById(R.id.transactions_filter);
        back_IV = (ImageView) findViewById(R.id.back_IV);
        /* Initialize recyclerview */
        mRecyclerView = (RecyclerView) findViewById(R.id.transactions_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(10));

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.showPassCode = false;
                finish();
            }
        });

        for(int i = 0; i < 20; i++){
            TransactionItem bean = new TransactionItem();
            bean.setStoreName("BIGBAZAAR "+i);
            bean.setCodeEntered("Code Entered : 9999 " + i);
            bean.setAddress2("#123, Road no : 1, Jubillehils, \nHyderabad - 500034 " + i);
            bean.setDate_tv("23rd OCT 2016 \n 15:00 pm " + i);
            bean.setOrderNum("123456789 " + i);
            bean.setAmount_rupees("1900"+i);
            bean.setTransac_status_text("Success "+i);
            feedItemList.add(bean);
        }

        adapter = new TranscationListAdapter(TransactionsActivity.this, feedItemList);
        mRecyclerView.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.showPassCode = false;
        finish();
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.getTransactions_methodName)) {
                Gson g = new Gson();
                /*SignInResponseModel model = g.fromJson(response, SignInResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {

                }else{
                    Display.DisplayToast(LoginActivity.this, model.message);
                }*/
            }
        }
    }

    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }
}
