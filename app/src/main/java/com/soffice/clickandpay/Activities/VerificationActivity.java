package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.VerificationResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class VerificationActivity extends AppCompatActivity implements TaskListner {

    CardView getStartedLayout;
    EditText otp_EditText;
    TextView resend_TV, why_verify_tv, phn_TV;
    String otp_code, className;
    ImageView back_IV, ok_IV;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();

        getStartedLayout = (CardView) findViewById(R.id.getStartedLayout);
        otp_EditText = (EditText) findViewById(R.id.otp_EditText);
        resend_TV = (TextView) findViewById(R.id.resend_TV);
        why_verify_tv = (TextView) findViewById(R.id.why_verify_tv);
        back_IV = (ImageView) findViewById(R.id.back_IV);
        ok_IV = (ImageView) findViewById(R.id.ok_IV);
        phn_TV = (TextView) findViewById(R.id.phn_TV);

        phn_TV.setText(session.getMobileNum());

        getStartedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        otp_EditText.setText(getIntent().getStringExtra("VERIFY_CODE"));

        otp_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Display.DisplayLogI("ADITYA", "onTextChanged onTextChanged " + count);
                otp_code = otp_EditText.getText().toString();
                if (otp_code.length() == 5) {
                    session = clickpay.getSession();
                    Display.DisplayLogI("ADITYA", "AUTH KEY " + session.getAuthKey());
                    if (session.getAuthKey() != null && session.getAuthKey().length() > 2) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("authkey", session.getAuthKey());
                                params.put("otp", otp_code);
                                requester.StringRequesterFormValues(urls.otpVerification, Request.Method.POST, className, urls.otpVerification_methodName, params);
                            }
                        }, 500);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Register")) {
                    Intent i = new Intent(VerificationActivity.this, RegisterActivity.class);
                    i.putExtra("fromActivity", "Login");
                    startActivity(i);
                    finish();
                }else if(getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Login")) {
                    Intent i = new Intent(VerificationActivity.this, LoginActivity.class);
                    i.putExtra("fromActivity", "Login");
                    startActivity(i);
                    finish();
                }else{
                    finish();
                }
            }
        });

        ok_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_code = otp_EditText.getText().toString();
                if (otp_code.length() == 5) {
                    Display.DisplayLogI("ADITYA", "AUTH KEY " + session.getAuthKey());
                    if (session.getAuthKey() != null && session.getAuthKey().length() > 2) {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("authkey", session.getAuthKey());
                        params.put("otp", otp_code);
                        requester.StringRequesterFormValues(urls.otpVerification, Request.Method.POST, className, urls.otpVerification_methodName, params);
                    }
                }
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(getIntent().getStringExtra("fromActivity") != null && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("Register")) {
            Intent i = new Intent(VerificationActivity.this, InitialActivity.class);
            i.putExtra("fromActivity", "Login");
            startActivity(i);
            finish();
        }else{
            finish();
        }
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.otpVerification_methodName)) {
                Gson g = new Gson();
                VerificationResponseModel model = g.fromJson(response, VerificationResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setAuthKey(model.key);

                    Intent i = new Intent(VerificationActivity.this, PassCodeActivity.class);
                    i.putExtra("fromActivity", "Verification");
                    startActivity(i);
                    finish();
                } else {
                    Display.DisplayToast(VerificationActivity.this, model.message);
                }
            }
        }
    }
}
