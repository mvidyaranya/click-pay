package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.ChangePasswordOrCodeResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class ChangePassWordOrPassCode extends AppCompatActivity implements TaskListner {

    TextView Label_Title_Activity, save_title;
    EditText current_word, new_word, conform_new_word;
    ImageView back_IV;
    int function = 0;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;
    String className;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_word_or_pass_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        className = getLocalClassName();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();

        Label_Title_Activity = (TextView) findViewById(R.id.Label_Title_Activity);
        save_title = (TextView) findViewById(R.id.save_title);

        current_word = (EditText) findViewById(R.id.current_word);
        new_word = (EditText) findViewById(R.id.new_word);
        conform_new_word = (EditText) findViewById(R.id.conform_new_word);/*
        current_word.setCursorVisible(false);
        new_word.setCursorVisible(false);
        conform_new_word.setCursorVisible(false);

        current_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_word.setCursorVisible(true);
            }
        });

        new_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new_word.setCursorVisible(true);
            }
        });

        conform_new_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conform_new_word.setCursorVisible(true);
            }
        });*/

        back_IV = (ImageView) findViewById(R.id.back_IV);

        Display.DisplayLogI("ADITYA", " from "+getIntent().getStringExtra("fromActivity"));

        if (getIntent().hasExtra("fromActivity") && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("changePassword")) {
            function = 1;
            Label_Title_Activity.setText("Change Password");
            current_word.setHint("Current Password");
            new_word.setHint("New Password");
            conform_new_word.setHint("Conform new Password");

        } else if (getIntent().hasExtra("fromActivity") && getIntent().getStringExtra("fromActivity").equalsIgnoreCase("changePasscode")) {
            function = 2;
            Label_Title_Activity.setText("Change Passcode");
            current_word.setHint("Current Passcode");
            new_word.setHint("New Passcode");
            conform_new_word.setHint("Conform new Passcode");
        }

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        save_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePhoneKeypad();
                if (function == 1) {
                    if (current_word.getText().toString().length() > 0) {
                        if (new_word.getText().toString().length() > 0) {
                            if (conform_new_word.getText().toString().length() > 0) {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("authkey", session.getAuthKey());
                                params.put("oldpass", current_word.getText().toString());
                                params.put("mid", session.getDeviceId());
                                params.put("pass", new_word.getText().toString());
                                params.put("repass", conform_new_word.getText().toString());
                                params.put("version", Constants.App_Version);
                                requester.StringRequesterFormValues(urls.changePassword, Request.Method.POST, className, urls.changePassword_methodName, params);
                            } else {
                                Display.DisplayToast(ChangePassWordOrPassCode.this, "Conform New Password");
                            }
                        } else {
                            Display.DisplayToast(ChangePassWordOrPassCode.this, "Enter New Password");
                        }
                    } else {
                        Display.DisplayToast(ChangePassWordOrPassCode.this, "Enter  current password");
                    }

                } else if (function == 2) {
                    if (current_word.getText().toString().length() > 0) {
                        if (new_word.getText().toString().length() > 0) {
                            if (conform_new_word.getText().toString().length() > 0) {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("authkey", session.getAuthKey());
                                params.put("oldpass", current_word.getText().toString());
                                params.put("mid", session.getDeviceId());
                                params.put("pass", new_word.getText().toString());
                                params.put("repass", conform_new_word.getText().toString());
                                params.put("version", Constants.App_Version);
                                requester.StringRequesterFormValues(urls.changePasscode, Request.Method.POST, className, urls.changePasscode_methodName, params);
                            } else {
                                Display.DisplayToast(ChangePassWordOrPassCode.this, "Conform New Passcode");
                            }
                        } else {
                            Display.DisplayToast(ChangePassWordOrPassCode.this, "Enter New Passcode");
                        }
                    } else {
                        Display.DisplayToast(ChangePassWordOrPassCode.this, "Enter current passcode");
                    }
                }
            }
        });


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && (_methodName.equalsIgnoreCase(urls.changePasscode_methodName) || _methodName.equalsIgnoreCase(urls.changePassword_methodName))) {
                Gson g = new Gson();
                ChangePasswordOrCodeResponseModel model = g.fromJson(response, ChangePasswordOrCodeResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    Display.DisplayToast(ChangePassWordOrPassCode.this, ""+model.message);
                    finish();
                } else {
                    Display.DisplayToast(ChangePassWordOrPassCode.this, "Error : " + response);
                }
            }
        }
    }

    public void removePhoneKeypad() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }
}
