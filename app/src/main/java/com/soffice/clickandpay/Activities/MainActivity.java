package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.soffice.clickandpay.Adapters.ClickandPayFragmentPagerAdapter;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.Fragments.HomeFragment;
import com.soffice.clickandpay.Fragments.RequestMoneyFragment;
import com.soffice.clickandpay.Fragments.SendMoneyFragment;
import com.soffice.clickandpay.Listeners.PageChangedToClearListener;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.UI.PagerSlidingTabStrip;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    ClickandPayFragmentPagerAdapter clicandPayAdp;
    PagerSlidingTabStrip tabsStrip;
    Toolbar toolbar;
    ClickandPay clickPay;
    SessionManager session;
    static boolean doubleClick = false;
    RelativeLayout rel_main_menu, payAtStore, transactionHistory_row, addMoney_row, sendMoney_row, requestdMoney_row, aboutUs_row;
    public static RelativeLayout alertBox;
    LinearLayout main_menu;
    ImageView ic_menu;
    public static ImageView img_alert;
    TextView profileSettings_edit, userName_sidemenu;
    public static TextView alertTextView, alertResponseTextView;
    PageChangedToClearListener clearSend, clearRequest, clearHome;
    public static boolean showPassCode = true;
    ScrollView sideMenu_scroll_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//
        clickPay = (ClickandPay) getApplicationContext();
        session = clickPay.getSession();
        session.setIsLoggedIn(true);

        rel_main_menu = (RelativeLayout) findViewById(R.id.rel_main_menu);
        main_menu = (LinearLayout) findViewById(R.id.main_menu);
        ic_menu = (ImageView) findViewById(R.id.ic_menu);
        profileSettings_edit = (TextView) findViewById(R.id.profileSettings_edit);
        userName_sidemenu = (TextView) findViewById(R.id.userName_sidemenu);
        payAtStore = (RelativeLayout) findViewById(R.id.payAtStore);
        alertBox = (RelativeLayout) findViewById(R.id.alertBox);
        img_alert = (ImageView) findViewById(R.id.img_alert);
        alertTextView = (TextView) findViewById(R.id.alertTextView);
        alertResponseTextView = (TextView) findViewById(R.id.alertResponseTextView);
        transactionHistory_row = (RelativeLayout) findViewById(R.id.transactionHistory_row);
        addMoney_row = (RelativeLayout) findViewById(R.id.addMoney_row);
        sendMoney_row = (RelativeLayout) findViewById(R.id.sendMoney_row);
        requestdMoney_row = (RelativeLayout) findViewById(R.id.requestdMoney_row);
        aboutUs_row = (RelativeLayout) findViewById(R.id.aboutUs_row);
        sideMenu_scroll_layout = (ScrollView) findViewById(R.id.sideMenu_scroll_layout);

        userName_sidemenu.setText(session.getUserName());

        doubleClick = false;

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        clicandPayAdp = new ClickandPayFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(clicandPayAdp);

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);

        tabsStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Display.DisplayLogI("ADITYA", "PAGE CHANGED " + position);
                SendMoneyFragment.canClear_send = true;
                RequestMoneyFragment.canClear_request = true;
                clearHome = HomeFragment.home;
                clearSend = SendMoneyFragment.sendMoney;
                clearRequest = RequestMoneyFragment.requestMoney;
                if (clearHome != null) {
                    clearHome.clearAll();
                }
                if (clearSend != null) {
                    clearSend.clearAll();
                }
                if (clearRequest != null) {
                    clearRequest.clearAll();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ic_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLeftDrawer("");
            }
        });


        rel_main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLeftDrawer("");
            }
        });

        main_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        profileSettings_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProfileSettings.class);
                startActivity(i);
                toggleLeftDrawer("");
            }
        });

        payAtStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLeftDrawer("");
            }
        });
        transactionHistory_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, TransactionsActivity.class);
                startActivity(i);
                toggleLeftDrawer("");
            }
        });
        addMoney_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AddMoneyActivity.class);
                startActivity(i);
                toggleLeftDrawer("");
            }
        });
        sendMoney_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(1, true);
                toggleLeftDrawer("");
            }
        });
        requestdMoney_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(2, true);
                toggleLeftDrawer("");
            }
        });
        aboutUs_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AboutUsActivity.class);
                startActivity(i);
                toggleLeftDrawer("");
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        userName_sidemenu.setText(session.getUserName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Display.DisplayLogI("ADITYA", "MAIN ACTIVITY RESTART");
        if(showPassCode) {
            Intent i = new Intent(MainActivity.this, PassCodeActivity.class);
            i.putExtra("fromActivity", "Login");
            startActivity(i);
            finish();
        }else{
            showPassCode = true;
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Display.DisplayLogI("ADITYA", "doubleClick "+doubleClick);
        if(doubleClick){
            finish();
        }else{
            doubleClick = true;
            Display.DisplayToast(MainActivity.this, "Press again to exit");
        }
    }


    public void toggleLeftDrawer(String s) {

        Display.DisplayLogI("ADITYA", "IN TOGGLE LEFT DRAWER");
        try {
            removePhoneKeypad();
            if (rel_main_menu.getVisibility() == View.VISIBLE) {
                Animation bottomdown = AnimationUtils.loadAnimation(this, R.anim.menu_right_in);
                main_menu.startAnimation(bottomdown);
                main_menu.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        rel_main_menu.setVisibility(View.GONE);
                    }
                }, 200);
            } else {
                sideMenu_scroll_layout.post(new Runnable() {
                    @Override
                    public void run() {

                        sideMenu_scroll_layout.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
                Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.menu_right_out);
                main_menu.startAnimation(bottomUp);
                main_menu.setVisibility(View.VISIBLE);
                rel_main_menu.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removePhoneKeypad() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }

}
