package com.soffice.clickandpay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.Adapters.CardsFragmentPagerAdapter;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.PaymentResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.UI.PagerSlidingTabStrip;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;

import java.util.HashMap;
import java.util.Map;

public class PaymentDetailsActivity extends AppCompatActivity implements TaskListner {

    ViewPager viewPager;
    PagerSlidingTabStrip tabsStrip;
    CardsFragmentPagerAdapter cardsAdp;
    String amountSpoending, payingAmount,className, vendorCode;
    TextView addMoney_TextView, cuttingMoney_TextView, fromWallet_Checkbox,pay_rupees;
    ImageView back_IV, checkBox;
    ClickandPay clickpay;
    SessionManager session;
    JsonRequester requester;
    Urls urls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        clickpay = (ClickandPay) getApplicationContext();
        session = clickpay.getSession();
        requester = new JsonRequester(this);
        urls = clickpay.getUrls();
        className = getLocalClassName();
        vendorCode = getIntent().getStringExtra("vendorCode");


        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) findViewById(R.id.viewpager_paymentDetails);
        cardsAdp = new CardsFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(cardsAdp);

        // Give the PagerSlidingTabStrip the ViewPager
        tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(viewPager);


        amountSpoending = getIntent().getStringExtra("amountSpend");
        addMoney_TextView = (TextView) findViewById(R.id.addMoney_TextView);
        cuttingMoney_TextView = (TextView) findViewById(R.id.cuttingMoney_TextView);
        back_IV = (ImageView) findViewById(R.id.back_IV);
        fromWallet_Checkbox = (TextView) findViewById(R.id.fromWallet_Checkbox);
        checkBox = (ImageView) findViewById(R.id.checkBox);
        pay_rupees = (TextView) findViewById(R.id.pay_rupees);

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fromWallet_Checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.getTag().toString().equalsIgnoreCase("checked")) {
                    checkBox.setImageResource(R.mipmap.ic_unchecked_checkbox);
                    checkBox.setTag("unchecked");
                } else {
                    checkBox.setImageResource(R.mipmap.ic_checkedeckbox);
                    checkBox.setTag("checked");
                }
            }
        });

        addMoney_TextView.setText(amountSpoending);
        cuttingMoney_TextView.setText(session.getWalletBal());
        if(Double.parseDouble(session.getWalletBal()) >=  Double.parseDouble(amountSpoending)){
            payingAmount = String.valueOf(Double.parseDouble(amountSpoending));
            pay_rupees.setText("Pay \u20B9 "+String.valueOf(Double.parseDouble(amountSpoending)));
        }else{
            if(Double.parseDouble(session.getWalletBal()) > 0){
                payingAmount = String.valueOf(Double.parseDouble(amountSpoending) - Double.parseDouble(session.getWalletBal()));
                pay_rupees.setText("Pay \u20B9 "+String.valueOf(Double.parseDouble(amountSpoending) - Double.parseDouble(session.getWalletBal())));
            }else{
                payingAmount = String.valueOf(Double.parseDouble(session.getWalletBal()) + Double.parseDouble(amountSpoending));
                pay_rupees.setText("Pay \u20B9 "+String.valueOf(Double.parseDouble(session.getWalletBal()) + Double.parseDouble(amountSpoending)));
            }
        }

        pay_rupees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceedToPay(payingAmount, vendorCode);
            }
        });

        back_IV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.showPassCode = false;
                finish();
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.showPassCode = false;
        finish();
    }

    public void proceedToPay(String amnt, String code){
        Map<String, String> params = new HashMap<String, String>();
        params.put("authkey", session.getAuthKey());
        params.put("vendercode", code);
        params.put("version", Constants.App_Version);
        params.put("mid", clickpay.getDeviceId(getApplicationContext()));
        params.put("amount", amnt);
        requester.StringRequesterFormValues(urls.payment, Request.Method.POST, className, urls.payment_methodName, params);
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(this, response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.payment_methodName)) {
                Gson g = new Gson();
                PaymentResponseModel model = g.fromJson(response, PaymentResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setWalletBal(model.wallet_bal);
                    Intent i = new Intent(PaymentDetailsActivity.this, PaymentSuccessActivity.class);
                    i.putExtra("fromActivity", "PaymentDetails");
                    i.putExtra("vendorCode", vendorCode);
                    i.putExtra("amount", payingAmount);
                    startActivity(i);
                    finish();
                } else {
                    Display.DisplayToast(this, model.message);
                }
            }
        }
    }
}
