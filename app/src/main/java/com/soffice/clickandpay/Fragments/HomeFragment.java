package com.soffice.clickandpay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.soffice.clickandpay.Activities.AddMoneyActivity;
import com.soffice.clickandpay.Activities.FullScannerActivity;
import com.soffice.clickandpay.Activities.PaymentDetailsActivity;
import com.soffice.clickandpay.Activities.PaymentSuccessActivity;
import com.soffice.clickandpay.ClickandPay;
import com.soffice.clickandpay.Listeners.PageChangedToClearListener;
import com.soffice.clickandpay.NetWork.JsonRequester;
import com.soffice.clickandpay.NetWork.TaskListner;
import com.soffice.clickandpay.NetWork.Urls;
import com.soffice.clickandpay.Pojo.MerchantPojo;
import com.soffice.clickandpay.Pojo.MerchentCheckResponseModel;
import com.soffice.clickandpay.Pojo.PaymentResponseModel;
import com.soffice.clickandpay.R;
import com.soffice.clickandpay.Utilty.Constants;
import com.soffice.clickandpay.Utilty.Display;
import com.soffice.clickandpay.Utilty.SessionManager;
import com.soffice.clickandpay.Utilty.SoftKeyboard;
import com.soffice.clickandpay.Utilty.SoftKeyboardStateHelper;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by sys2025 on 13/9/15.
 */
public class HomeFragment extends Fragment implements PageChangedToClearListener, SoftKeyboardStateHelper.SoftKeyboardStateListener, TaskListner {
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String CODE_ENTERED = "CODE_ENTERED";
    public static String fromActivity = null;
    public static String code_in_QR = null;
    public static HomeFragment home;

    private int mPage;
    LinearLayout scan_QR, addMoneyLayout, proceedAddMoneyLayout,cashBackLayout;
    TextView Pay_addMoney, or;
    public  static TextView walletBal_Home;
    EditText vendorCode1, vendorCode2, vendorCode3, vendorCode4, addMoney_EditText;
    String vc1, vc2, vc3, vc4, amountEntered, className, vendorCode;
    TextView storeName, codeEntered, address1, cashBack_offer;
    TableRow addMoney_tableRow;
    ImageView closeProceedAddMoney, marchntLogo;
    String ss = "<font color=#000000>";
    String ss1 = "Code : ";
    String ss2 = "</font> <font color=#bf0e14>";
    String ss3 = "Ameerpet road, Hyderabad - 500034";
    String ss4 = "</font>";
    String ss5 = "GS 24, Carat Building,";
    String ss6 = "BIGBAZAR Super Market";
    String ss7 = "Address : ";
    ClickandPay clickpay;
    SessionManager session;
    SoftKeyboard softKeyboard;
    SoftKeyboardStateHelper softKeyboardStateHelper;
    JsonRequester requester;
    Urls urls;

    public static HomeFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    // Inflate the fragment layout we defined above for this fragment
    // Set the associated text for the title
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        // Get the ViewPager and set it's PagerAdapter so that it can display items

        clickpay = (ClickandPay) getActivity().getApplicationContext();
        session = clickpay.getSession();
        home = this;

        scan_QR = (LinearLayout) view.findViewById(R.id.scan_QR);

        vendorCode1 = (EditText) view.findViewById(R.id.vendorCode1);
        vendorCode2 = (EditText) view.findViewById(R.id.vendorCode2);
        vendorCode3 = (EditText) view.findViewById(R.id.vendorCode3);
        vendorCode4 = (EditText) view.findViewById(R.id.vendorCode4);
        Pay_addMoney = (TextView) view.findViewById(R.id.Pay_addMoney);
        walletBal_Home = (TextView) view.findViewById(R.id.walletBal_Home);
        addMoney_EditText = (EditText) view.findViewById(R.id.addMoney_EditText);
        or = (TextView) view.findViewById(R.id.or);

        storeName = (TextView) view.findViewById(R.id.storeName);
        codeEntered = (TextView) view.findViewById(R.id.codeEntered);
        address1 = (TextView) view.findViewById(R.id.address1);
        addMoneyLayout = (LinearLayout) view.findViewById(R.id.addMoney);
        proceedAddMoneyLayout = (LinearLayout) view.findViewById(R.id.proceedAddMoney);
        closeProceedAddMoney = (ImageView) view.findViewById(R.id.closeProceedAddMoney);
        marchntLogo = (ImageView) view.findViewById(R.id.marchntLogo);
        cashBack_offer = (TextView) view.findViewById(R.id.cashBack_offer);
        cashBackLayout = (LinearLayout) view.findViewById(R.id.cashBackLayout);
        addMoney_tableRow = (TableRow) view.findViewById(R.id.addMoney_tableRow);

        softKeyboardStateHelper = new SoftKeyboardStateHelper(view.findViewById(R.id.rootView));
        softKeyboardStateHelper.addSoftKeyboardStateListener(this);

        requester = new JsonRequester(this);
        urls = clickpay.getUrls();
        className = getActivity().getLocalClassName();

        vendorCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    vendorCode2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vendorCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    vendorCode3.requestFocus();
                } else if (count == 0) {
                    vendorCode1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vendorCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    vendorCode4.requestFocus();
                } else if (count == 0) {
                    vendorCode2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vendorCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1) {
                    vc1 = vendorCode1.getText().toString();
                    vc2 = vendorCode2.getText().toString();
                    vc3 = vendorCode3.getText().toString();
                    vc4 = vendorCode4.getText().toString();
                    amountEntered = addMoney_EditText.getText().toString();
                    if (vc1.length() > 0 && vc2.length() > 0 && vc3.length() > 0 && vc4.length() > 0) {

                        vendorCode = vc1 + vc2 + vc3 + vc4;
                        checkMerchant(vc1 + vc2 + vc3 + vc4);
                        Display.DisplayLogI("ADITYA", "ENTERED CODE : " + vc1 + vc2 + vc3 + vc4);

                    } else {
                        Display.DisplayToast(getActivity(), "Enter a proper Vendor Code..!!");
                        vendorCode1.requestFocus();
                    }
                } else if (count == 0) {
                    vendorCode3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addMoney_tableRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddMoneyActivity.class);
                i.putExtra("fromActivity", "HomeFrag");
                startActivity(i);
            }
        });

        vendorCode1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.requestFocus();
            }
        });


        scan_QR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FullScannerActivity.class));
            }
        });

        closeProceedAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMoneyLayout.setVisibility(View.VISIBLE);
                or.setVisibility(View.VISIBLE);
                proceedAddMoneyLayout.setVisibility(View.GONE);
                closeProceedAddMoney.setVisibility(View.GONE);

                fromActivity = null;
                code_in_QR = null;
                vendorCode1.setText("");
                vendorCode2.setText("");
                vendorCode3.setText("");
                vendorCode4.setText("");
//                addMoney_EditText.setText("");
//                addMoney_EditText.setHint("0.00");
                cashBackLayout.setVisibility(View.GONE);

                vendorCode1.clearFocus();
                vendorCode2.clearFocus();
                vendorCode3.clearFocus();
                vendorCode4.clearFocus();
                addMoney_EditText.clearFocus();

            }
        });

        walletBal_Home.setText(session.getWalletBal());

        addMoney_EditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (!addMoney_EditText.getText().toString().contains(".")) {
                        if (addMoney_EditText.getText().toString().length() == 0) {
                            addMoney_EditText.setText(addMoney_EditText.getText().toString() + "0.00");
                        } else {
                            addMoney_EditText.setText(addMoney_EditText.getText().toString() + ".00");
                        }
                    } else {
                        String[] str = addMoney_EditText.getText().toString().split("\\.");
                        if (str.length > 1) {
                            if (str[1].toString().length() == 1) {
                                addMoney_EditText.setText(addMoney_EditText.getText().toString() + "0");
                            } else if (str[1].toString().length() > 2) {
                                addMoney_EditText.setText(str[0] + "." + str[1].substring(0, 2));
                                Display.DisplayLogI("ADITYA", "str[1].substring(0, 2) " + str[1].substring(0, 2));
                            } else if (str[1].toString().length() == 0) {
                                addMoney_EditText.setText(addMoney_EditText.getText().toString() + ".00");
                            }
                        }
                    }
                }else{
                    addMoney_EditText.setText("");
                }
            }
        });

        addMoney_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (addMoney_EditText.getText().toString().length() == 0) {
//                    addMoney_EditText.setText("");
//                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (addMoney_EditText.getText().toString().length() > 0 && Double.parseDouble(addMoney_EditText.getText().toString()) > Double.parseDouble(session.getWalletBal())) {
                        Pay_addMoney.setText("Proceed to Pay");
                    } else {
                        Pay_addMoney.setText("Pay");
                    }
                    Display.DisplayLogI("ADITYA", " AMOUNTTT " + s.toString());
                    if (addMoney_EditText.getText().toString().contains(".")) {
                        s.toString().split(".");
                        Display.DisplayLogI("ADITYA", " SPLITTT " + s.toString().split("\\.").length);
                        String[] str = addMoney_EditText.getText().toString().split("\\.");
                        if (str.length > 1) {
                            Display.DisplayLogI("ADITYA", " SPLITTT PARTS " + str[0]+ "  "+str[1]);
                            if (str[1].toString().length() > 2) {
                                addMoney_EditText.setText(str[0] + "." + str[1].substring(0, 2));
                                Display.DisplayLogI("ADITYA", "str[1].substring(0, 2) " + str[1].substring(0, 2));
                                removePhoneKeypad();
                                addMoney_EditText.clearFocus();
                            }
                        }
                    }

                    if(addMoney_EditText.getText().toString().length() > 0 && Double.parseDouble(addMoney_EditText.getText().toString()) > 0) {
                        if (vendorCode != null && vendorCode.length() > 0) {
                            cashBack_offer.setText("*10% Cashback Rs. " + ((Double.parseDouble(addMoney_EditText.getText().toString()) * 10) / 100) + " will be added to your wallet \n after successful transaction.");
                            cashBackLayout.setVisibility(View.VISIBLE);
                        }
                    }else{
                        cashBackLayout.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    addMoney_EditText.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Pay_addMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Pay_addMoney.getText().toString().equalsIgnoreCase("Pay")) {
                    vc1 = vendorCode1.getText().toString();
                    vc2 = vendorCode2.getText().toString();
                    vc3 = vendorCode3.getText().toString();
                    vc4 = vendorCode4.getText().toString();
                    amountEntered = addMoney_EditText.getText().toString();
                    if (vc1.length() > 0 && vc2.length() > 0 && vc3.length() > 0 && vc4.length() > 0) {
                        vendorCode = vc1 + vc2 + vc3 + vc4;
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("authkey", session.getAuthKey());
                        params.put("vendercode", vendorCode);
                        params.put("version", Constants.App_Version);
                        params.put("mid", clickpay.getDeviceId(getActivity().getApplicationContext()));
                        params.put("amount", addMoney_EditText.getText().toString());
                        requester.StringRequesterFormValues(urls.payment, Request.Method.POST, className, urls.payment_methodName, params);

                    } else {
                        if(vendorCode != null && vendorCode.length() > 0){
                            Display.DisplayLogI("ADITYA", "VVCODE "+vendorCode);
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("authkey", session.getAuthKey());
                            params.put("vendercode", vendorCode);
                            params.put("version", Constants.App_Version);
                            params.put("mid", clickpay.getDeviceId(getActivity().getApplicationContext()));
                            params.put("amount", addMoney_EditText.getText().toString());
                            requester.StringRequesterFormValues(urls.payment, Request.Method.POST, className, urls.payment_methodName, params);

                        }else{
                            Display.DisplayToast(getActivity(), "Enter a proper Vendor Code..!!");
                        }
                    }

                }else if(Pay_addMoney.getText().toString().equalsIgnoreCase("Proceed to Pay")){
                    vc1 = vendorCode1.getText().toString();
                    vc2 = vendorCode2.getText().toString();
                    vc3 = vendorCode3.getText().toString();
                    vc4 = vendorCode4.getText().toString();
                    amountEntered = addMoney_EditText.getText().toString();
                    if (vc1.length() > 0 && vc2.length() > 0 && vc3.length() > 0 && vc4.length() > 0) {
                        vendorCode = vc1 + vc2 + vc3 + vc4;
//                        proceedToAddMoney(model.merchat);
                        if (addMoney_EditText.getText().toString().length() > 0 && Double.parseDouble(addMoney_EditText.getText().toString()) > 1.00) {
                            Intent i = new Intent(getActivity(), PaymentDetailsActivity.class);
                            i.putExtra("fromActivity", "HomeFrag");
                            i.putExtra("amountSpend", addMoney_EditText.getText().toString());
                            i.putExtra("vendorCode", vendorCode);
                            startActivity(i);
                        } else {
                            Display.DisplayToast(getActivity(), "Enter amount");
                        }
                    } else {
                        Display.DisplayToast(getActivity(), "Enter a proper Vendor Code..!!");
                        vendorCode1.requestFocus();
                    }
                }
            }
        });

        return view;
    }


    public void checkMerchant(String code){
        Map<String, String> params = new HashMap<String, String>();
        params.put("poscode", code);
        requester.StringRequesterFormValues(urls.merchant, Request.Method.POST, className, urls.merchant_methodName, params);
    }

    public void proceedToAddMoney(MerchantPojo merchat){
        addMoneyLayout.setVisibility(View.GONE);
        or.setVisibility(View.GONE);
        storeName.setText(merchat.merchantStore);
        Picasso.with(getActivity()).load(merchat.merchantLogo).into(marchntLogo);
        address1.setText(merchat.merchantAddress);
        proceedAddMoneyLayout.setVisibility(View.VISIBLE);
        closeProceedAddMoney.setVisibility(View.VISIBLE);
        fromActivity = null;
        addMoney_EditText.clearFocus();
        Pay_addMoney.requestFocus();
//        addMoney_EditText.setCursorVisible(false);
        removePhoneKeypad();
        if(addMoney_EditText.getText().toString().length() > 0 && Double.parseDouble(addMoney_EditText.getText().toString()) > 0) {
            cashBack_offer.setText("*10% Cashback Rs. " + ((Double.parseDouble(addMoney_EditText.getText().toString()) * 10) / 100) + "will be added to your wallet \\n after successful transaction.");
            cashBackLayout.setVisibility(View.VISIBLE);
        }else{
            cashBackLayout.setVisibility(View.GONE);
        }


//        addMoney_EditText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addMoney_EditText.setCursorVisible(true);
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(fromActivity != null && fromActivity.equalsIgnoreCase("QRScan")) {
            if(code_in_QR != null && code_in_QR.length() == 4) {
                codeEntered.setText(Html.fromHtml(ss + ss1 + ss2 + vendorCode + ss4));
                Display.DisplayLogI("ADITYA", "code_in_QR :: " + code_in_QR);

                vendorCode = code_in_QR;
                checkMerchant(vendorCode);
//                proceedToAddMoney();
            }
        }

        vendorCode1.clearFocus();
        vendorCode2.clearFocus();
        vendorCode3.clearFocus();
        vendorCode4.clearFocus();
        addMoney_EditText.clearFocus();
    }



    @Override
    public void onSoftKeyboardOpened(int keyboardHeightInPx) {

    }

    @Override
    public void onSoftKeyboardClosed() {
        addMoney_EditText.clearFocus();
//        addMoney_EditText.setCursorVisible(false);
    }

    public void removePhoneKeypad() {

        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }

    @Override
    public void onTaskfinished(String response, int cd, String _className, String _methodName) {
        Display.DisplayLogI("ADITYA", "" + response);
        if (cd == 00) {
            Display.DisplayToast(getActivity(), response);
        } else if (cd == 05) {
            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.merchant_methodName)) {
                Gson g = new Gson();
                MerchentCheckResponseModel model = g.fromJson(response, MerchentCheckResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {

                    codeEntered.setText(Html.fromHtml(ss + ss1 + ss2 + model.merchat.pos_code + ss4));
                    Display.DisplayLogI("ADITYA", "pos_code "+model.merchat.pos_code);
                    proceedToAddMoney(model.merchat);
                    Pay_addMoney.requestFocus();
                } else {
                    Display.DisplayToast(getActivity(), model.message);
                    clearAll();
                }
            }

            if (_className.equalsIgnoreCase(className) && _methodName.equalsIgnoreCase(urls.payment_methodName)) {
                Gson g = new Gson();
                PaymentResponseModel model = g.fromJson(response, PaymentResponseModel.class);
                Display.DisplayLogI("ADITYA code", model.code);
                if (model.code.equalsIgnoreCase("200")) {
                    session.setWalletBal(model.wallet_bal);
                    Intent i = new Intent(getActivity(), PaymentSuccessActivity.class);
                    i.putExtra("fromActivity", "PaymentDetails");
                    i.putExtra("vendorCode", vendorCode);
                    i.putExtra("amount", addMoney_EditText.getText().toString());
                    startActivity(i);
                    getActivity().finish();
                } else {
                    Display.DisplayToast(getActivity(), model.message);
                }
            }
        }
    }

    @Override
    public void clearAll() {
        vendorCode1.setText("");
        vendorCode2.setText("");
        vendorCode3.setText("");
        vendorCode4.setText("");
        addMoney_EditText.setText("");

        vendorCode1.clearFocus();
        vendorCode2.clearFocus();
        vendorCode3.clearFocus();
        vendorCode4.clearFocus();
        addMoney_EditText.clearFocus();
    }
}
