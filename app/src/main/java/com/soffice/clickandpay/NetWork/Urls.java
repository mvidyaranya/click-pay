package com.soffice.clickandpay.NetWork;

/**
 * Created by aditya on 11/2/16.
 */
public class Urls {

    /* Home URLS */
    public  static final String homeUrl = "http://54.169.8.171/cnpmobile/api/";

    /* SignUp URLS */
    public static final String userSignUp_methodName = "userSignUp";
    public static final String userSignUp = homeUrl+"userSignUp";

    /* Signin URLS*/
    public static final String userSignIn_methodName = "userLogin";
    public static final String userSignIn = homeUrl+"userLogin";

    /* Verification URLS*/
    public static final String otpVerification_methodName = "otpVerification";
    public static final String otpVerification = homeUrl+userSignUp_methodName+"/otpVerification";

    /* Set Passcode URLS*/
    public static final String passcode_methodName = "passcode";
    public static final String passcode = homeUrl+"passcode";

    /* Chech Passcode URLS */
    public static final String check_passcode_methodName = "checkPasscode";
    public static final String check_passcode = homeUrl+passcode_methodName+"/checkPasscode";

    /* Marchant URLS */
    public static final String merchant_methodName = "merchant";
    public static final String merchant = homeUrl+"merchant";

    /* Payment URLS */
    public static final String payment_methodName = "payment";
    public static final String payment = homeUrl+"payment";

    /* Feedback URLS */
    public static final String feedback_methodName = "feedback";
    public static final String feedback = homeUrl+"feedback";

    /* Send Money URLS */
    public static final String sendMoney_methodName = "sendMoney";
    public static final String sendMoney = homeUrl+"sendMoney";

    /* Request Money URLS */
    public static final String requestMoney_methodName = "requestMoney";
    public static final String requestMoney = homeUrl+"requestMoney";

    /* Forgot Password URLS */
    public static final String forgotPassword_methodName = "forgotPassword";
    public static final String forgotPassword = homeUrl+"forgotPassword";

    /* Forgot Passcode URLS */
    public static final String forgotPasscode_methodName = "forgotPasscode";
    public static final String forgotPasscode = homeUrl+"forgotPasscode";

    /* Change Password URLS */
    public static final String changePassword_methodName = "changePassword";
    public static final String changePassword = homeUrl+"changePassword";

    /* Change Passcode URLS */
    public static final String changePasscode_methodName = "changePasscode";
    public static final String changePasscode = homeUrl+"changePasscode";

    /* Get Profile URLS */
    public static final String getProfile_methodName = "Profile";
    public static final String getProfile = homeUrl+"Profile";

    /* Update Profile URLS */
    public static final String updateProfile_methodName = getProfile_methodName+"update";
    public static final String updateProfile = homeUrl+getProfile_methodName+"/update";

    /* Get Transactions URLS */
    public static final String getTransactions_methodName = "Transactions";
    public static final String getTransactions = homeUrl+"Transactions";


}