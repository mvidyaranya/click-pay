package com.soffice.clickandpay.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.soffice.clickandpay.Pojo.TransactionItem;
import com.soffice.clickandpay.R;

import java.util.List;

/**
 * Created by sys2033 on 24/2/16.
 */
public class TranscationListAdapter extends RecyclerView.Adapter<FeedListRowHolder> {

    private List<TransactionItem> feedItemList;

    private Context mContext;

    public TranscationListAdapter(Context context, List<TransactionItem> feedItemList) {
        this.feedItemList = feedItemList;
        this.mContext = context;
    }

    @Override
    public FeedListRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.transaction_item, null);
        FeedListRowHolder mh = new FeedListRowHolder(v);

        return mh;
    }

    @Override
    public void onBindViewHolder(FeedListRowHolder feedListRowHolder, int i) {
        TransactionItem feedItem = feedItemList.get(i);

        Glide.with(mContext).load(feedItem.getThumbnail())
                .error(R.mipmap.ic_bigbazaar_logo)
                .placeholder(R.mipmap.ic_bigbazaar_logo)
                .into(feedListRowHolder.thumbnail);

        feedListRowHolder.transac_status_logo.setImageResource(R.mipmap.tick_green);
        feedListRowHolder.storeName.setText(feedItem.getStoreName());
        feedListRowHolder.codeEntered.setText(feedItem.getCodeEntered());
        feedListRowHolder.date_tv.setText(feedItem.getDate_tv());
        feedListRowHolder.address2.setText(feedItem.getAddress2());
        feedListRowHolder.orderNum.setText(feedItem.getOrderNum());
        feedListRowHolder.amount_rupees.setText(feedItem.getAmount_rupees());
        feedListRowHolder.transac_status_text.setText(feedItem.getTransac_status_text());

        if(i%10 == 0) {
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.BLUE);
        }else if(i % 10 == 1){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.CYAN);
        }else if(i % 10 == 2){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.GREEN);
        }else if(i % 10 == 3){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.RED);
        }else if(i % 10 == 4){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.MAGENTA);
        }else if(i % 10 == 5){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.YELLOW);
        }else if(i % 10 == 6){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.CYAN);
        }else if(i % 10 == 7){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.BLUE);
        }else if(i % 10 == 8){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.YELLOW);
        }else if(i % 10 == 9){
            feedListRowHolder.colorStyle_TV.setBackgroundColor(Color.MAGENTA);
        }
    }

    @Override
    public int getItemCount() {
        return (null != feedItemList ? feedItemList.size() : 0);
    }
}

class FeedListRowHolder extends RecyclerView.ViewHolder {
    protected ImageView thumbnail, transac_status_logo;
    protected TextView date_tv, storeName, codeEntered, address2, orderNum, amount_rupees, transac_status_text,colorStyle_TV;

    public FeedListRowHolder(View view) {
        super(view);
        this.thumbnail = (ImageView) view.findViewById(R.id.vendorlogo);
        this.transac_status_logo = (ImageView) view.findViewById(R.id.transac_status_logo);
        this.date_tv = (TextView) view.findViewById(R.id.date_tv);
        this.storeName = (TextView) view.findViewById(R.id.storeName);
        this.codeEntered = (TextView) view.findViewById(R.id.codeEntered);
        this.address2 = (TextView) view.findViewById(R.id.address2);
        this.orderNum = (TextView) view.findViewById(R.id.orderNum);
        this.amount_rupees = (TextView) view.findViewById(R.id.amount_rupees);
        this.transac_status_text = (TextView) view.findViewById(R.id.transac_status_text);
        this.colorStyle_TV = (TextView) view.findViewById(R.id.colorStyle_TV);
    }

}